package com.rm.office_bing.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.Utils.ValidationClass;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener, ResponseCarrier, NetworkCallbacker
{
	private static final String TAG = "ForgetPasswordActivity";
	private final int forgetPasswordIdentifier = 1002;
	Button btnRequestOtp;
	NetworkCaller networkCaller;
	SessionManager sessionManager;
	ProgressDialogView progressDialogView;
	TextInputEditText editEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forget_password);
		//Preprocessing();
		initViews();
		initVariables();
		initListeners();
	}

	private void initViews()
	{
		btnRequestOtp = findViewById(R.id.btn_req_otp);
		editEmail = findViewById(R.id.et_email_address);
	}

	private void initVariables()
	{
		progressDialogView = new ProgressDialogView(this);
		sessionManager = new SessionManager(this);
		networkCaller = new NetworkCaller(this, this);
	}

	private void initListeners()
	{
		btnRequestOtp.setOnClickListener(this);
	}

	private void Preprocessing()
	{
		new OpenMenu(this, (AppCompatImageView) findViewById(R.id.img_menu), R.menu.home_option_menu, false);
		new BackButton(this, (AppCompatImageView) findViewById(R.id.img_back), true);
		new SetHeader((AppCompatTextView) findViewById(R.id.txt_header), "Forgot Password", true);
		InternetStatus.getInstance().isOnline(this, (AppCompatImageView) findViewById(R.id.img_internet_status), true);
		NetworkConnectionBrods.getInstance().setData(this, this);
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		Preprocessing();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btn_req_otp:
				if (isVerified())
				{
					ProceedToForgetPasswordCall();
				}
				break;
		}
	}

	private boolean isVerified()
	{
		return ValidationClass.isValidEmail(editEmail);
	}

	private void ProceedToForgetPasswordCall()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.ForgotPassword, getBodyForForgetPassword(), forgetPasswordIdentifier, false);
	}

	private JsonObject getBodyForForgetPassword()
	{
		JsonObject object = new JsonObject();
		object.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		object.addProperty(ConstantVariables.ApiValues.StringEmailKey, editEmail.getText().toString());
		return object;
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case forgetPasswordIdentifier:
				if (ResponseValidator.isValidResponse(Response, this, true))
				{
					Intent intent = new Intent(this, LoginActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				}
		}
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case forgetPasswordIdentifier:
				Toast.makeText(this, "Error Occurred.! Please Try Again Later ", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "Error:Response:forgetPasswordIdentifier " + Response);
				break;
		}
	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(this, (AppCompatImageView) findViewById(R.id.img_internet_status), true);
	}
}
