package com.rm.office_bing.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.CommonClasses.SaveProfileData;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.fragments.HomeFragments.HomeFragment;
import com.rm.office_bing.fragments.HomeFragments.InviteFragment;
import com.rm.office_bing.fragments.HomeFragments.PlanFragment;
import com.rm.office_bing.fragments.HomeFragments.ProfileFragment;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import static com.rm.office_bing.Utils.ConstantVariables.BottomNavigationIds.MenuHome;
import static com.rm.office_bing.Utils.ConstantVariables.BottomNavigationIds.MenuPlan;
import static com.rm.office_bing.Utils.ConstantVariables.BottomNavigationIds.MenuProfile;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemReselectedListener, ResponseCarrier, ReplaceFragment, NetworkCallbacker
{

	private static final String TAG = "HomeActivity";
	final int FrameHolder = R.id.frame_switch_frags;
	private final int profileIdentifier = 456;
	Fragment homeFragment, inviteFragment, planFragment, profileFragment;
	BottomNavigationView bottomNavigationView;
	int count = 0;
	Handler handler;
	NetworkCaller networkCaller;
	SessionManager sessionManager;
	ProgressDialogView progressDialogView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		initWidgets();
		initVariables();
		getProfileData();
		initListeners();
		startDefault();
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		Preprocessing();
	}

	private void getProfileData()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.MyProfile, getBodyForProfileFetch(), profileIdentifier, true);
	}

	private JsonObject getBodyForProfileFetch()
	{
		JsonObject object = new JsonObject();
		object.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		Log.d(TAG, "getBodyForProfileFetch:object " + object);
		return object;
	}

	private void Preprocessing()
	{
		new BackButton(this, findViewById(R.id.img_back), false);
		new OpenMenu(this, findViewById(R.id.img_menu), R.menu.home_option_menu, true);
		InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
		NetworkConnectionBrods.getInstance().setData(this, this);
	}

	private void startDefault()
	{
		switchFragment(homeFragment);
		ChangeHeader(ConstantVariables.CommonValues.StringHome);
	}

	private void initListeners()
	{
		bottomNavigationView.setOnNavigationItemReselectedListener(this);
		bottomNavigationView.setOnNavigationItemSelectedListener(this);

	}

	private void initVariables()
	{
		ChangeHeader(ConstantVariables.CommonValues.StringHome);
		homeFragment = new HomeFragment(this);
		planFragment = new PlanFragment(this);
		inviteFragment = new InviteFragment(this);
		profileFragment = new ProfileFragment(this);
		handler = new Handler(getMainLooper());
		networkCaller = new NetworkCaller(this, this);
		sessionManager = new SessionManager(this);
		progressDialogView = new ProgressDialogView(this);
	}

	private void ChangeHeader(String header)
	{
		new SetHeader((AppCompatTextView) findViewById(R.id.txt_header), header, true);
	}

	private void initWidgets()
	{
		bottomNavigationView = findViewById(R.id.bottomNavigationView);
	}

	void switchFragment(Fragment fragment)
	{
		getSupportFragmentManager().beginTransaction().replace(FrameHolder, fragment).commit();

	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item)
	{
		switch (item.getItemId())
		{
			case MenuHome:
				switchFragment(homeFragment);
				ChangeHeader(ConstantVariables.CommonValues.StringHome);
				return true;

			case MenuProfile:
				switchFragment(profileFragment);
				ChangeHeader(ConstantVariables.CommonValues.StringProfile);
				return true;
		}
		return false;
	}

	@Override
	public void onNavigationItemReselected(@NonNull MenuItem item)
	{
	}

	@Override
	public void onBackPressed()
	{
		count++;
		if (count >= 2)
		{
			finishAffinity();
		}
		if (count == 1)
		{
			Toast.makeText(this, "Press Again To Exit", Toast.LENGTH_SHORT).show();
		}
		handler.postDelayed(() -> count = 0, 3000);
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case profileIdentifier:
				if (ResponseValidator.isValidResponse(Response, this, false))
				{
					SaveProfileData.init().startSaving(Response, sessionManager);

				}
				break;
		}
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case profileIdentifier:
				Log.d(TAG, "Error: Error Getting Profile Data");
				break;
		}
	}

	@Override
	public void replace(Fragment fragment)
	{
		Log.d(TAG, "replace:fragment ");
		//final int fragmentId=fragment.getId();
		bottomNavigationView.setSelectedItemId(MenuPlan);

	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
		if (status)
		{
			initVariables();
			startDefault();
			bottomNavigationView.setSelectedItemId(MenuHome);
		}
	}
}
