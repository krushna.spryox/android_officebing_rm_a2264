package com.rm.office_bing.Activities;

import android.Manifest;
import android.Manifest.permission;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.ImageViewCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.LeadsModels.LeadDetails.LeadDetailsDataItem;
import com.rm.office_bing.Models.LeadsModels.LeadDetails.LeadDetailsResponse;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.Utils.ToastMessages;
import com.rm.office_bing.databinding.ActivityAcceptedLeadsBinding;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

public class LeadsDetailsActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener, ResponseCarrier, NetworkCallbacker
{
    String TAG="LeadsDetailsActivity";
    ActivityAcceptedLeadsBinding binding;
    FloatingActionButton imgOpenContacts, floatCall, floatMail;
    AppCompatImageView imgMenu;
    LinearLayoutCompat linearCopyNumber, linearCopyMail;
    AppCompatTextView txtViewTimeline, txtAddFollowups, txtPhoneNumber, txtEmail;
    ConstraintLayout constraintLocations;
	NetworkCaller networkCaller;
	ProgressDialogView progressDialogView;
	SessionManager sessionManager;
    private final int leadDetailsIdentifier=192;
    ConstraintLayout constraintShowDispute;
    String ClientName;
    FrameLayout frameNoData;
    CoordinatorLayout coordinateHasData;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding=ActivityAcceptedLeadsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //Preprocessing();
        initWidgets();
        initVariables();
        initListeners();
	    getLeadDetails();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Preprocessing();

    }

	private void getLeadDetails()
	{
		progressDialogView.showProgress();

		networkCaller.initPostCallWithBody(ApiNodes.LeadDetails,getRequestBodyForLeadDetails(),leadDetailsIdentifier,true);
	}

	private JsonObject getRequestBodyForLeadDetails()
    {
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty(ConstantVariables.ApiValues.StringLeadIdKey,getLeadId());
        jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey,getSpacePartnerId());
        jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey,sessionManager.getUserID());
        return jsonObject;
    }

    private int getLeadId()
	{
		return getIntent().getIntExtra(ConstantVariables.ApiValues.StringLeadIdKey,-1);
	}
	private int getSpacePartnerId()
	{
		return getIntent().getIntExtra(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey,-1);
	}


	private void Preprocessing()
    {
        new BackButton(this, findViewById(R.id.img_back), true);
        new OpenMenu(this, findViewById(R.id.img_menu), R.menu.home_option_menu, false);
        new SetHeader(findViewById(R.id.txt_header), ClientName, true);
	    InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
	    NetworkConnectionBrods.getInstance().setData(this,this);
    }

    private void initWidgets()
    {
        imgMenu = findViewById(R.id.img_menu);
        frameNoData= findViewById(R.id.frame_no_data);
        coordinateHasData= findViewById(R.id.coordinate_has_data);
        constraintShowDispute = findViewById(R.id.constraint_show_dispute);
        imgOpenContacts = findViewById(R.id.open_contact_details);
        txtViewTimeline = findViewById(R.id.txt_view_timeline);
        txtAddFollowups = findViewById(R.id.txt_add_followups);
        constraintLocations = findViewById(R.id.constraintLayout_location_prefs);
        floatCall = findViewById(R.id.float_call);
        floatMail = findViewById(R.id.float_mail);
        linearCopyMail = findViewById(R.id.linear_copy_email);
        linearCopyNumber = findViewById(R.id.linear_copy_number);
        txtPhoneNumber = findViewById(R.id.txt_user_call);
        txtEmail = findViewById(R.id.txt_user_email);
    }

    private void showHideContactViews()
    {
        if (floatCall.getVisibility() == View.GONE)
        {
            floatCall.setVisibility(View.VISIBLE);
            floatMail.setVisibility(View.VISIBLE);
            imgOpenContacts.setImageResource(R.drawable.img_close);
            ImageViewCompat.setImageTintList(imgOpenContacts, ColorStateList.valueOf(getResources().getColor(R.color.white)));
        }
        else
        {
            floatCall.setVisibility(View.GONE);
            floatMail.setVisibility(View.GONE);
            imgOpenContacts.setImageResource(R.drawable.img_profile);
            ImageViewCompat.setImageTintList(imgOpenContacts, ColorStateList.valueOf(getResources().getColor(R.color.white)));
        }
    }


    private void initVariables()
    {
		networkCaller=new NetworkCaller(this,this);
		progressDialogView=new ProgressDialogView(this);
        sessionManager=new SessionManager(this);
    }

    private void initListeners()
    {
        imgOpenContacts.setOnClickListener(this);
        txtViewTimeline.setOnClickListener(this);
        txtAddFollowups.setOnClickListener(this);
	    floatCall.setOnClickListener(this);
        constraintLocations.setOnClickListener(this);
        floatMail.setOnClickListener(this);
        linearCopyMail.setOnLongClickListener(this);
        linearCopyNumber.setOnLongClickListener(this);
        constraintShowDispute.setOnClickListener(this);
        binding.frameNoData.setOnClickListener(v->getLeadDetails());
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.float_call:
                Intent intentCall = new Intent(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:"+txtPhoneNumber.getText().toString()));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(this, "Call Permission Required.", Toast.LENGTH_SHORT).show();
                    if (VERSION.SDK_INT >= VERSION_CODES.M)
                    {
                        requestPermissions(new String[]{permission.CALL_PHONE}, 123);
                    }
                    return;
                }
                startActivity(intentCall);
                break;
            case R.id.float_mail:
                Intent intentMail = new Intent(Intent.ACTION_SENDTO);
                intentMail.setData(Uri.parse("mailto:"+txtEmail.getText().toString()));
                startActivity(intentMail);
            break;
            case R.id.open_contact_details:
                showHideContactViews();
                break;
            case R.id.constraintLayout_location_prefs:
                Intent locationIntent = new Intent(this, TimelineActivity.class);
                locationIntent.putExtra(ConstantVariables.IntentValues.ViewLocations, ConstantVariables.IntentValues.ViewLocations);
                //startActivity(intent);
                break;
            case R.id.txt_view_timeline:
                Intent intentTimeline = new Intent(this, TimelineActivity.class);
                intentTimeline.putExtra(ConstantVariables.IntentValues.ViewTimeline, ConstantVariables.IntentValues.ViewTimeline);
                startActivity(intentTimeline);
                break;
            case R.id.txt_add_followups:
                Intent intentFollowup = new Intent(this, TimelineActivity.class);
                intentFollowup.putExtra(ConstantVariables.IntentValues.AddFollowup, ConstantVariables.IntentValues.AddFollowup);
                startActivity(intentFollowup);
                break;
            case R.id.constraint_show_dispute:
            Intent intentDispute = new Intent(this, TimelineActivity.class);
                intentDispute.putExtra(ConstantVariables.IntentValues.ViewDisputes, ConstantVariables.IntentValues.ViewDisputes);
            startActivity(intentDispute);
            break;
        }
    }

    @Override
    public boolean onLongClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_copy_number:
                Copy(txtPhoneNumber.getText().toString(),"Phone Number");
                return true;
            case R.id.linear_copy_email:
                Copy(txtEmail.getText().toString(),"Email");
                return true;
        }
        return false;
    }

    private void Copy(String copyString,String Type)
    {
        ClipboardManager clipboardManager= (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData data=ClipData.newPlainText("copied",copyString);
        if(clipboardManager!=null)
        {
            clipboardManager.setPrimaryClip(data);
            Toast.makeText(this, Type+" copied", Toast.LENGTH_SHORT).show();
        }
    }

	@Override
	public void Success(String Response, int Identifier)
	{
        progressDialogView.hideProgress();

        switch (Identifier)
        {
            case leadDetailsIdentifier:
                if(ResponseValidator.isValidResponse(Response,this,false))
                ExtractLeadDetails(Response);
                else
                    NOData();
                break;
        }
	}

    private void NOData()
    {
        frameNoData.setVisibility(View.VISIBLE);
        coordinateHasData.setVisibility(View.GONE);
    }

    private void ExtractLeadDetails(String response)
    {
        LeadDetailsDataItem data=null;
        try
        {
            LeadDetailsResponse leadDetailsResponse=new Gson().fromJson(response,LeadDetailsResponse.class);
            if(leadDetailsResponse.getData()!=null&&leadDetailsResponse.getData().size()>0)
            {
                data = leadDetailsResponse.getData().get(0);
                HasData();

            }
            else
                NOData();
        }
        catch (Exception e)
        {
            Log.d(TAG, "ExtractLeadDetails:Errror Extracting Lead Details "+e.getLocalizedMessage());
	        NOData();
        }
        if(data!=null)
        UIUpDate(data);

    }

    private void HasData()
    {
        frameNoData.setVisibility(View.GONE);
        coordinateHasData.setVisibility(View.VISIBLE);
    }

    private void UIUpDate(@NotNull LeadDetailsDataItem data)
    {
        ClientName=data.getFirstName()+" "+data.getLastName();
        Preprocessing();
        binding.txtUserCall.setText(data.getMobile());
        binding.txtUserEmail.setText(data.getEmail());
        binding.txtName.setText(ClientName);
        binding.txtTentStartDate.setText(data.getTentativeDate());
        binding.txtDate.setText(data.getTentativeDate());
        binding.txtUserCompanyType.setText(data.getOfficeType());
        binding.txtNoSeats.setText(String.valueOf(data.getSeats()));
        binding.txtBudget.setText("Rs."+data.getBudget());
        binding.txtTypeSpace.setText(data.getOfficeType());
        if(data.getCityName()!=null)
        binding.txtLocationNames.setText(data.getCityName());
        else
            binding.txtLocationNames.setText(ToastMessages.StringNoCityPrefs);

        if(data.getDisputeReasonStatus()!=null)
        binding.txtDisputeReason.setText(data.getDisputeReasonStatus());
        else
            binding.txtDisputeReason.setText(ToastMessages.StringNoDispute);

	    sessionManager.setSpaceLeadId(String.valueOf(getSpacePartnerId()));
	    sessionManager.setLeadId(String.valueOf(getLeadId()));

    }



    @Override
	public void Error(Throwable Response, int Identifier)
	{
        progressDialogView.hideProgress();
        NOData();

	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		sessionManager.setSpaceLeadId("-1");
		sessionManager.setLeadId("-1");
	}

    @Override
    public void StateChange(boolean status)
    {
        InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
    }
}
