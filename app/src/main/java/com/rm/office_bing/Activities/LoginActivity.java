package com.rm.office_bing.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.Utils.ValidationClass;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements ResponseCarrier, View.OnClickListener
{
	private static final String TAG = "LoginActivity";

	private final int loginConstant = 1001;
	TextView txtForgetPassword;
	NetworkCaller networkCaller;
	SessionManager manager;
	ProgressDialogView progressDialogView;
	AppCompatCheckBox checkRememberMe;
	private TextView tv_signup;
	private TextInputEditText et_emailid, et_password;
	private Button btn_login;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initViews();
		initVariables();
		initListeners();
		setData();
	}

	private void setData()
	{
		btn_login.setText(R.string.login_btn_text);
	}

	private void initViews()
	{
		et_emailid = findViewById(R.id.et_emailid);
		checkRememberMe = findViewById(R.id.check_remember_me);
		et_password = findViewById(R.id.et_password);
		tv_signup = findViewById(R.id.txt_signin);
		btn_login = findViewById(R.id.btn_login);
		txtForgetPassword = findViewById(R.id.txt_forget_password);
	}

	private void initListeners()
	{
		tv_signup.setOnClickListener(this);
		btn_login.setOnClickListener(this);
		txtForgetPassword.setOnClickListener(this);
	}


	private void initVariables()
	{
		networkCaller = new NetworkCaller(this, this);
		manager = new SessionManager(this);
		progressDialogView = new ProgressDialogView(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.txt_signin:
				//   startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
				break;

			case R.id.btn_login:
				if (isValid())
				{
					progressDialogView.showProgress();
					networkCaller.initPostCallWithBody(ApiNodes.Login, getRequestBodyForLogin(), loginConstant, false);
				}
				break;
			case R.id.txt_forget_password:
				startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
				break;
		}
	}

	private JsonObject getRequestBodyForLogin()
	{
		JsonObject object = new JsonObject();
		object.addProperty(ConstantVariables.ApiValues.StringEmailKey, Objects.requireNonNull(et_emailid.getText()).toString());
		object.addProperty(ConstantVariables.ApiValues.StringPassKey, Objects.requireNonNull(et_password.getText()).toString());
		object.addProperty(ConstantVariables.ApiValues.StringFcmKey, Objects.requireNonNull(manager.getFcmToken()));
		Log.d(TAG, "getRequestBodyForRegistration: object  " + object);
		return object;
	}

	private boolean isValid()
	{
		boolean NonEmptyFields = checkIfFieldsEmpty();
		boolean value = NonEmptyFields && ValidationClass.isValidEmail(et_emailid) && ValidationClass.isValidPassword(et_password);
		Log.d(TAG, "isValid: " + value);
		return value;
	}

	private boolean checkIfFieldsEmpty()
	{
		List<AppCompatEditText> list = new ArrayList<>();
		list.add(et_emailid);
		list.add(et_password);
		return !ValidationClass.isListEmpty(list);
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case loginConstant:
                if (ResponseValidator.isValidResponse(Response, this, true))
                {
                    ExtractLoginData(Response);
                }
				break;
		}
	}

	private void ExtractLoginData(String response)
	{
		progressDialogView.hideProgress();
		try
		{
			JSONObject object = new JSONObject(response);
			JSONObject Data = new JSONObject(object.optString("data"));
			manager.setAPIToken(Data.optString("token"));
			startActivity(new Intent(LoginActivity.this, MyLeadsActivity.class));
			manager.setLogin(checkRememberMe.isChecked());
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		Log.e(TAG, "LoginResponseError" + Response);
	}
}
