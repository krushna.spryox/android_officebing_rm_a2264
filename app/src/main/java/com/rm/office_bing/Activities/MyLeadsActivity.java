package com.rm.office_bing.Activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.LeadsAdapter.LeadsAdapter;
import com.rm.office_bing.Adapters.LeadsAdapter.LeadsTypesAdapter;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Communicators.SerializedPassing;
import com.rm.office_bing.Models.LeadsModels.LeadsAdapterModel;
import com.rm.office_bing.Models.LeadsModels.ListLeads.ListLeadsItem;
import com.rm.office_bing.Models.LeadsModels.ListLeads.ListLeadsResponse;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.KeyAccepted;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.KeyDiscarded;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.KeyPending;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.StringAccepted;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.StringDiscarded;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.StringPending;

public class MyLeadsActivity extends AppCompatActivity implements View.OnClickListener, SerializedPassing, ResponseCarrier, NetworkCallbacker
{
	//objects
	final int leadsTypeIdentifier = 1001;
	private final bottomSheetCallBack callBack = new bottomSheetCallBack();
	private final int getLeadList = 1001;
	String TAG = "MyLeadsActivity";
	//activity views
	Context context;
	RecyclerView recyclerLeads;
	//bottomsheet views
	RecyclerView recyclerLeadRadios;
	BottomSheetBehavior<LinearLayoutCompat> bottomSheetBehavior;
	LinearLayoutCompat linearLayoutBottomSheet;
	AppCompatImageView imgUpDown;
	Handler handler;
	List<LeadsAdapterModel> leadsTypesModelList;
	NetworkCaller networkCaller;
	SessionManager manager;
	ProgressBar progressDialogView;
	int PageCount = 1;
	AppCompatTextView txtNoData;
	boolean hasToRemoveViewsFirst = false;
	LeadsAdapter adapter;
	ProgressBar progressLoading;
	private List<ListLeadsItem> leadsDataToFill;
	private LinearLayoutManager LeadsManager;
	int count = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_leads);
		//Preprocessing();
		initWidgets();
		initVariables();
		initListeners();
		setNonEmptyDataToLeadTypeList();

	}

	private void Preprocessing()
	{
		new BackButton(this, findViewById(R.id.img_back), false);
		new OpenMenu(this, findViewById(R.id.img_menu), R.menu.home_option_menu, true);
		InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
		new SetHeader(findViewById(R.id.txt_header), "Leads", true);
		NetworkConnectionBrods.getInstance().setData(this,this);
	}
	@Override
	protected void onStart()
	{
		super.onStart();
		Preprocessing();
		getLeadByType(false);
	}

	private void getLeadByType(boolean isRefreshing)
	{
		if(!isRefreshing)
		progressDialogView.setVisibility(View.VISIBLE);
		networkCaller.initPostCallWithBody(ApiNodes.MyLeads, getRequestBodyForHomeDetails(getType(leadsTypesModelList)), getLeadList, true);
	}

	private String getType(List<LeadsAdapterModel> leadsAdapterModelList)
	{
        for (LeadsAdapterModel model : leadsAdapterModelList)
        {
            if (model.getChecked())
            {
	            //new SetHeader(findViewById(R.id.txt_header), model.getTitle()+" Leads", true);
                return model.getKey();
            }
        }
		return null;
	}

	private JsonObject getRequestBodyForHomeDetails(String status)
	{
		JsonObject object = new JsonObject();
		object.addProperty(ConstantVariables.ApiValues.StringIdKey, manager.getUserID());
		//object.addProperty(ConstantVariables.ApiValues.StringTypeKey, status);
		object.addProperty(ConstantVariables.ApiValues.StringPageKey, PageCount);
		return object;
	}

	private void initListeners()
	{
		bottomSheetBehavior.addBottomSheetCallback(callBack);
		linearLayoutBottomSheet.setOnClickListener(this);
		recyclerLeads.setOnScrollListener(new RecyclerScroll());
	}

	private void initVariables()
	{
		PageCount = 1;
		context = this;
		leadsTypesModelList = new ArrayList<>();
		leadsDataToFill = new ArrayList<>();
		bottomSheetBehavior = BottomSheetBehavior.from(linearLayoutBottomSheet);
		handler = new Handler(getMainLooper());
		networkCaller = new NetworkCaller(context, this);
		manager = new SessionManager(context);

		LeadsManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
	}

	private void initWidgets()
	{

		recyclerLeads = findViewById(R.id.recycler_leads);
		progressLoading = findViewById(R.id.progress_loading);
		progressDialogView=findViewById(R.id.progress_bar);
		linearLayoutBottomSheet = findViewById(R.id.linear_bottom_sheet);
		imgUpDown = findViewById(R.id.img_up_down);
		recyclerLeadRadios = findViewById(R.id.recycler_lead_radios);
		txtNoData = findViewById(R.id.txt_no_data);
	}


	private void setNonEmptyDataToLeadTypeList()
	{
		recyclerLeadRadios.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
		LeadsTypesAdapter adapter = new LeadsTypesAdapter(context, this, leadsTypeIdentifier, getNonEmptyLeadTypeList());
		recyclerLeadRadios.setAdapter(adapter);
	}

	private List<LeadsAdapterModel> getNonEmptyLeadTypeList()
	{
		leadsTypesModelList.clear();
		leadsTypesModelList.add(new LeadsAdapterModel(StringPending, false, KeyPending));
		leadsTypesModelList.add(new LeadsAdapterModel(StringDiscarded, false, KeyDiscarded));
		leadsTypesModelList.add(new LeadsAdapterModel(StringAccepted, true, KeyAccepted));
		return leadsTypesModelList;
	}

	private void setNonEmptyDataToLeadList()
	{

        if (hasToRemoveViewsFirst && recyclerLeads.getAdapter() != null)
        {
            recyclerLeads.removeAllViews();
        }
		if (PageCount > 1)
		{
			DoneRefresh();
			adapter.setData(leadsDataToFill);
			LeadsManager.scrollToPosition(leadsDataToFill.size() - 1);
			return;
		}
		recyclerLeads.setLayoutManager(LeadsManager);
		adapter = new LeadsAdapter(context, leadsDataToFill);
		recyclerLeads.setAdapter(adapter);
	}


	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.linear_bottom_sheet:
				BottomSheetManager();
				break;
		}
	}

	private void BottomSheetManager()
	{
		if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
		{
			bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
		}
		else
		{
			bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
		}
	}

	@Override
	public void PassData(Serializable serializable, int Identifier)
	{
		switch (Identifier)
		{
			case leadsTypeIdentifier:
				hasToRemoveViewsFirst = true;
				PageCount = 1;
				BottomSheetManager();
				leadsTypesModelList = (List<LeadsAdapterModel>) serializable;
				Log.d(TAG, "PassData: leadsAdapterModelList " + leadsTypesModelList.toString());
				getLeadByType(false);
				break;
		}
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		switch (Identifier)
		{
			case getLeadList:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractLeadsListData(Response);
				}
				else
				{
					NoLeadsData();
				}
				break;
		}
	}

	private void ExtractLeadsListData(String response)
	{
		try
		{
			//leadsDataToFill.clear();
			//recyclerLeads.removeAllViews();
			ListLeadsResponse listLeadsResponse = new Gson().fromJson(response, ListLeadsResponse.class);
			if (listLeadsResponse.getData() == null)
			{
				Log.d(TAG, "ExtractHomeData: listLeadsResponse.getData()==null");
				return;
			}
			leadsDataToFill = listLeadsResponse.getData().getLeads();
			if (leadsDataToFill.size() > 0)
			{
				LeadsData();
				setNonEmptyDataToLeadList();
				PageCount++;
			}
			else
			{
				NoLeadsData();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e(TAG, "HomeDetailsException" + e);
			NoLeadsData();
		}
		progressDialogView.setVisibility(View.GONE);

	}

	void NoLeadsData()
	{
		if (PageCount == 1)
		{
			recyclerLeads.setVisibility(View.GONE);
			txtNoData.setVisibility(View.VISIBLE);
		}
		else
		{
			DoneRefresh();
		}
	}

	private void DoneRefresh()
	{
		progressLoading.setVisibility(View.GONE);
	}

	void LeadsData()
	{
		recyclerLeads.setVisibility(View.VISIBLE);
		txtNoData.setVisibility(View.GONE);
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.setVisibility(View.GONE);
		switch (Identifier)
		{
			case getLeadList:
				NoLeadsData();
				break;
		}

	}

	private void Refreshing()
	{
		progressLoading.setVisibility(View.VISIBLE);
	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(this, findViewById(R.id.img_internet_status), true);
		if(status)
			getLeadByType(false);
	}

	class bottomSheetCallBack extends BottomSheetBehavior.BottomSheetCallback
	{
		@Override
		public void onStateChanged(@NonNull View bottomSheet, int newState)
		{
			switch (newState)
			{
				case BottomSheetBehavior.STATE_EXPANDED:
					imgUpDown.setImageDrawable(getResources().getDrawable(R.drawable.img_down));

					break;

				case BottomSheetBehavior.STATE_COLLAPSED:
					imgUpDown.setImageDrawable(getResources().getDrawable(R.drawable.img_up));
					break;
			}
		}

		@Override
		public void onSlide(@NonNull View bottomSheet, float slideOffset)
		{

		}
	}

	class RecyclerScroll extends RecyclerView.OnScrollListener
	{
		@Override
		public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState)
		{
			if (LeadsManager.findLastVisibleItemPosition() == adapter.modelList.size() - 1)
			{
				Refreshing();
				getLeadByType(true);
			}
			super.onScrollStateChanged(recyclerView, newState);
		}
	}

	@Override
	public void onBackPressed()
	{
		count++;
		if (count >= 2)
		{
			finishAffinity();
		}
		if (count == 1)
		{
			Toast.makeText(this, "Press Again To Exit", Toast.LENGTH_SHORT).show();
		}
		handler.postDelayed(() -> count = 0, 3000);
		//super.onBackPressed();
	}
}

