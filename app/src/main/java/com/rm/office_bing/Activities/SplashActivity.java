package com.rm.office_bing.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.rm.office_bing.R;
import com.rm.office_bing.Utils.SessionManager;

public class SplashActivity extends AppCompatActivity
{
	Handler handler;
	SessionManager manager;
	private int TIME_OUT = 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_avtivity);
		manager = new SessionManager(this);

		handler = new Handler();
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				if (manager.isLoggedIn())
				{
					Intent intent = new Intent(SplashActivity.this, MyLeadsActivity.class);
					startActivity(intent);
					finish();
				}
				else
				{
					Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
					startActivity(intent);
					finish();
				}
			}
		}, TIME_OUT);

	}
}
