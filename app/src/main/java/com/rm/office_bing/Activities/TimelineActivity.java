package com.rm.office_bing.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.fragments.LeadDetailsFragments.AddFollowupTimeLine;
import com.rm.office_bing.fragments.LeadDetailsFragments.SelectDisputesFragment;
import com.rm.office_bing.fragments.LeadDetailsFragments.ViewTimelineFragment;


public class TimelineActivity extends AppCompatActivity implements ReplaceFragment
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        getData();
    }

    private void getData()
    {
        if (getIntent().hasExtra(ConstantVariables.IntentValues.ViewTimeline))
        {
            getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(R.id.frame_timeline_holder, new ViewTimelineFragment(this)).commit();
        }
        else if (getIntent().hasExtra(ConstantVariables.IntentValues.ViewDisputes))
        {
            getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(R.id.frame_timeline_holder, new SelectDisputesFragment()).commit();
        }
        else
        {
            getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(R.id.frame_timeline_holder, new AddFollowupTimeLine(this)).commit();
        }
    }

    @Override
    public void replace(Fragment fragment)
    {
        getSupportFragmentManager().beginTransaction().disallowAddToBackStack().replace(R.id.frame_timeline_holder, fragment).commit();
    }
}
