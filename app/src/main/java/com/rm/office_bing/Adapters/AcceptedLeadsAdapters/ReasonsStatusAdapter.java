package com.rm.office_bing.Adapters.AcceptedLeadsAdapters;

import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.rm.office_bing.Communicators.DialogCommunicator;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsDataItem;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.rm.office_bing.Utils.ConstantVariables.ApiValues.StringClientWonVal;
import static com.rm.office_bing.Utils.ToastMessages.AddRevenueError;
import static com.rm.office_bing.Utils.ToastMessages.AdditionaCommnt;
import static com.rm.office_bing.Utils.ToastMessages.RevennueTexxt;
import static com.rm.office_bing.Utils.ToastMessages.RevenueError;

public class ReasonsStatusAdapter extends RecyclerView.Adapter<ReasonsStatusAdapter.AddFollowupViewholder>
{
	String TAG = "AddFollowupAdapter";
	CompoundButton ButtonView = null;
	SessionManager manager;
	NetworkCaller networkCaller;
	ProgressDialogView progressDialogView;
	int position = -1;
	String type;
	DialogCommunicator communicator;
	int Identifier;
	private List<ListReasonsDataItem> listReasons;
	private Context context;

	public ReasonsStatusAdapter(List<ListReasonsDataItem> listFollowups, Context context, String type)
	{
		this.listReasons = listFollowups;
		this.context = context;
		this.type = type;
	}

	public ReasonsStatusAdapter(List<ListReasonsDataItem> listFollowups, Context context, String type, DialogCommunicator communicator, int Identifier)
	{
		this.listReasons = listFollowups;
		this.context = context;
		this.type = type;
		this.communicator = communicator;
		this.Identifier = Identifier;
	}

	@NonNull
	@Override
	public AddFollowupViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new AddFollowupViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell_checkbox, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull AddFollowupViewholder holder, int position)
	{
		ListReasonsDataItem model = listReasons.get(position);
		holder.checkBox.setText(model.getName());
	}

	@Override
	public int getItemCount()
	{
		if (listReasons != null)
		{
			return listReasons.size();
		}
		else
		{
			return 0;
		}
	}

	class AddFollowupViewholder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener, ResponseCarrier, View.OnClickListener
	{
		final int StatusIdentifier = 120;
		CheckBox checkBox;
		LinearLayoutCompat linearFollowupCell, linearAddtionalComments;
		AppCompatEditText editAddtional;
		AppCompatButton btnAction;
		AppCompatTextView cmt_additional;
		public AddFollowupViewholder(@NonNull View itemView)
		{
			super(itemView);
			checkBox = itemView.findViewById(R.id.check);
			cmt_additional = itemView.findViewById(R.id.cmt_additional);
			linearFollowupCell = itemView.findViewById(R.id.followup_cell);
			linearAddtionalComments = itemView.findViewById(R.id.linear_comment);
			editAddtional = itemView.findViewById(R.id.et_additional_comments);
			btnAction = itemView.findViewById(R.id.btn_add_to_timeline);
			if (type.equals(ConstantVariables.ApiValues.StringFollowupType))
			{
				btnAction.setText("Add To Timeline");
			}
			else if (type.equals(ConstantVariables.ApiValues.StringDeclineType))
			{
				btnAction.setText("Reject Lead");
			}
			else
			{
				btnAction.setText("Mark Dispute");
			}
			checkBox.setOnCheckedChangeListener(this);
			manager = new SessionManager(context);
			progressDialogView = new ProgressDialogView(context);
			networkCaller = new NetworkCaller(context, this);
			btnAction.setOnClickListener(this);

		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
			if (ButtonView != null)
			{
				ButtonView.setChecked(false);
				hideComments();
			}
			if (isChecked)
			{
				int pos=getAdapterPosition();
				if(listReasons.get(pos).getCode_name().equalsIgnoreCase(StringClientWonVal))
				{
					editAddtional.setInputType(InputType.TYPE_CLASS_NUMBER);
					cmt_additional.setText(RevennueTexxt);
				}
				else
				{
					editAddtional.setInputType(InputType.TYPE_CLASS_TEXT);
					cmt_additional.setText(AdditionaCommnt);
				}
				showComments();
				ButtonView = buttonView;
			}
			else
			{
				hideComments();
				ButtonView = null;
			}

		}

		private void hideComments()
		{
			linearFollowupCell.setBackgroundColor(context.getResources().getColor(R.color.white));
			linearAddtionalComments.setVisibility(View.GONE);
		}

		private void showComments()
		{
			linearFollowupCell.setBackgroundColor(context.getResources().getColor(R.color.grey_20));
			linearAddtionalComments.setVisibility(View.VISIBLE);
		}

		@Override
		public void Success(String Response, int Identifier)
		{
			progressDialogView.hideProgress();
			switch (Identifier)
			{
				case StatusIdentifier:
					if (ResponseValidator.isValidResponse(Response, context, true))
					{
						ButtonView.setChecked(false);
						hideComments();
						listReasons.remove(position);
						position = -1;
						notifyDataSetChanged();
					}
					break;
			}
		}

		@Override
		public void Error(Throwable Response, int Identifier)
		{
			progressDialogView.hideProgress();
			switch (Identifier)
			{
				case StatusIdentifier:
					break;
			}
		}

		@Override
		public void onClick(View v)
		{
			switch (v.getId())
			{
				case R.id.btn_add_to_timeline:
					progressDialogView.showProgress();
					if (type.equals(ConstantVariables.ApiValues.StringFollowupType))
					{
						networkCaller.initPostCallWithBody(ApiNodes.AddFollowup, getBodyForUpdateOrAddStatus(), StatusIdentifier, true);
					}
					else if (type.equals(ConstantVariables.ApiValues.StringDeclineType))
					{
						int position = getAdapterPosition();
						listReasons.get(position).setComment(editAddtional.getText().toString());
						communicator.whatDialogSaid(listReasons.get(position), true, Identifier);

					}
					else
					{
						networkCaller.initPostCallWithBody(ApiNodes.MarkDispute, getBodyForUpdateOrAddStatus(), StatusIdentifier, true);
					}
					break;
			}
		}

		@NotNull
		private JsonObject getBodyForUpdateOrAddStatus()
		{
			position = getAdapterPosition();
			JsonObject object = new JsonObject();
			object.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, manager.getSpaceLeadId());
			object.addProperty(ConstantVariables.ApiValues.StringLeadIdKey, manager.getLeadId());
			if( listReasons.get(position).getCode_name().equalsIgnoreCase(StringClientWonVal))
			{
				if (isInteger(editAddtional))
					object.addProperty(ConstantVariables.ApiValues.StringRevenueKey, editAddtional.getText().toString());
				else
				{
					Toast.makeText(context, RevenueError, Toast.LENGTH_SHORT).show();
					editAddtional.setError(AddRevenueError);
					return null;
				}
			}
			else
			{
				if(editAddtional.getText()!=null&&!editAddtional.getText().toString().equalsIgnoreCase(""))
				{
					object.addProperty(ConstantVariables.ApiValues.StringCommentKey, editAddtional.getText().toString());
				}
			}
			if (type.equals(ConstantVariables.ApiValues.StringFollowupType))
			{
				object.addProperty(ConstantVariables.ApiValues.StringFollowupStatusIdKey, listReasons.get(position).getId());
			}
			else
			{
				object.addProperty(ConstantVariables.ApiValues.StringDisputeStatusIdKey, listReasons.get(position).getId());
			}
			Log.d(TAG, "getBodyForAddFOllowup: object " + object);
			return object;
		}
	}

	private boolean isInteger(AppCompatEditText value)
	{
		try
		{
			if(value!=null&&value.getText()!=null&&!value.getText().toString().equalsIgnoreCase(""))
			{
				double val = Double.parseDouble(value.getText().toString());
				return true;
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "isInteger: Errorconverting to number "+e.getLocalizedMessage());
		}
		return false;
	}
}
