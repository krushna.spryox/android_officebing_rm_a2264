package com.rm.office_bing.Adapters.AcceptedLeadsAdapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;


public class SelectCitiesAdapter extends RecyclerView.Adapter<SelectCitiesAdapter.SelectCitiesViewHolder>
{
	String TAG = "SelectCitiesAdapter";
	int localCount = 0;

	@NonNull
	@Override
	public SelectCitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new SelectCitiesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell_checkbox, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull SelectCitiesViewHolder holder, int position)
	{

	}

	@Override
	public int getItemCount()
	{
		return 10;
	}

	class SelectCitiesViewHolder extends RecyclerView.ViewHolder
	{
		CheckBox check;

		public SelectCitiesViewHolder(@NonNull View itemView)
		{
			super(itemView);
			check = itemView.findViewById(R.id.check);
			check.setOnCheckedChangeListener((buttonView, isChecked) ->
					{
						if (isChecked)
						{
							localCount++;
						}
						else
						{
							localCount--;
						}
						if (localCount > ConstantVariables.CommonValues.MaxCitiesCanSelect)
						{
							buttonView.setChecked(false);
							localCount--;
						}
						Log.d(TAG, "onCheckedChanged: " + localCount);
						Log.d(TAG, "onCheckedChanged: " + isChecked);
					}
			);
		}
	}
}
