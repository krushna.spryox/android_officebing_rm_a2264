package com.rm.office_bing.Adapters.AcceptedLeadsAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Models.TimelineModels.ListTimeline.ListTimelineDataItem;
import com.rm.office_bing.R;

import java.util.List;

public class ViewTimelineAdapter extends RecyclerView.Adapter<ViewTimelineAdapter.ViewTimelineHolder>
{
	Context context;
	private List<ListTimelineDataItem> listTimeline;

	public ViewTimelineAdapter(List<ListTimelineDataItem> listTimeline, Context context)
	{
		this.listTimeline = listTimeline;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewTimelineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new ViewTimelineHolder(LayoutInflater.from(context).inflate(R.layout.recycler_cell_view_timeline, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull ViewTimelineHolder holder, int position)
	{
		ListTimelineDataItem model = listTimeline.get(position);
		holder.txtTitle.setText(model.getFollowupStatus());
		holder.txtDesc.setText(model.getComment());
		holder.txtDateMonth.setText(getDate(model.getDate()));
		holder.txtYear.setText(getYear(model.getDate()));
		if (position == listTimeline.size() - 1)
		{
			holder.imgDown.setVisibility(View.GONE);
		}
	}

	private String getDate(String date)
	{
		String[] dates = date.split(" ");
		if (date.length() > 2)
		{
			return dates[0] + " " + dates[1];
		}
		else
		{
			return "";
		}
	}

	private String getYear(String date)
	{
		String[] dates = date.split(" ");
		if (date.length() > 2)
		{
			return dates[2];
		}
		else
		{
			return "";
		}
	}

	@Override
	public int getItemCount()
	{
		if (listTimeline != null)
		{
			return listTimeline.size();
		}
		else
		{
			return 0;
		}
	}

	class ViewTimelineHolder extends RecyclerView.ViewHolder
	{
		AppCompatImageView imgDown, imgUP;
		AppCompatTextView txtDateMonth;
		AppCompatTextView txtTitle, txtDesc, txtYear;

		public ViewTimelineHolder(@NonNull View itemView)
		{
			super(itemView);
			imgDown = itemView.findViewById(R.id.img_down);
			imgUP = itemView.findViewById(R.id.img_up);
			txtDateMonth = itemView.findViewById(R.id.txt_date_month);
			txtTitle = itemView.findViewById(R.id.txt_order_id);
			txtDesc = itemView.findViewById(R.id.txt_desc);
			txtYear = itemView.findViewById(R.id.txt_year);
		}
	}
}
