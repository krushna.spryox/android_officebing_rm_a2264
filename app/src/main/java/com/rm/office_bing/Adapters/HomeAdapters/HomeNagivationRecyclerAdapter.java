package com.rm.office_bing.Adapters.HomeAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rm.office_bing.Models.HomeModels.TextWithImageModel;
import com.rm.office_bing.R;

import java.util.List;

public class HomeNagivationRecyclerAdapter extends RecyclerView.Adapter<HomeNagivationRecyclerAdapter.NavigationViewHolder>
{

	List<TextWithImageModel> modelList;
	Context context;

	public HomeNagivationRecyclerAdapter(List<TextWithImageModel> modelList, Context context)
	{
		this.modelList = modelList;
		this.context = context;
	}

	@NonNull
	@Override
	public NavigationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new NavigationViewHolder(LayoutInflater.from(context).inflate(R.layout.image_text_vertical, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull NavigationViewHolder holder, int position)
	{
		TextWithImageModel model = modelList.get(position);
		holder.txtHolder.setText(model.getTitle());
		if (model.getImageUrl() != null)
		{
			Glide.with(context).load(model.getImageUrl()).circleCrop().into(holder.imgHolder);
		}
		else if (model.getResourceId() != -1)
		{
			holder.imgHolder.setImageResource(model.getResourceId());
		}

	}

	@Override
	public int getItemCount()
	{
		if (modelList != null)
		{
			return modelList.size();
		}
		else
		{
			return 0;
		}
	}

	class NavigationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
		AppCompatTextView txtHolder;
		AppCompatImageView imgHolder;
		LinearLayoutCompat linearLayoutNavigate;

		NavigationViewHolder(@NonNull View itemView)
		{
			super(itemView);
			txtHolder = itemView.findViewById(R.id.txt_holder);
			imgHolder = itemView.findViewById(R.id.img_holder);
			linearLayoutNavigate = itemView.findViewById(R.id.linear_navigate);
			linearLayoutNavigate.setOnClickListener(this);
		}

		@Override
		public void onClick(View v)
		{
			switch (v.getId())
			{
				case R.id.linear_navigate:
					final int pos = getAdapterPosition();
					context.startActivity(modelList.get(pos).getIntent());
					break;
			}
		}
	}
}
