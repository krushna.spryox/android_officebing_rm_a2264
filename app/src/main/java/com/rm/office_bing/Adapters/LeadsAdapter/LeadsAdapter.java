package com.rm.office_bing.Adapters.LeadsAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Activities.LeadsDetailsActivity;
import com.rm.office_bing.Adapters.AcceptedLeadsAdapters.ReasonsStatusAdapter;
import com.rm.office_bing.Communicators.DialogCommunicator;
import com.rm.office_bing.Communicators.DialogType;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsDataItem;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsResponse;
import com.rm.office_bing.Models.LeadsModels.ListLeads.ListLeadsItem;
import com.rm.office_bing.Models.OfficeBingDialogModel;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.OfficeBingDilogs;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.Utils.ToastMessages;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;


public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.LeadsViewholder> implements ResponseCarrier, DialogCommunicator
{
	private static final String TAG = "LeadsAdapter";
	final int AcceptLeadNWIdentifier = 1023;
	final int RejectLeadNWIdentifier = 1024;
	final int DismissListIdentifier = 10123;
	final int AcceptLeadByDialogIdentify = 102;
	public List<ListLeadsItem> modelList;
	NetworkCaller networkCaller;
	int Position = -1;
	ProgressDialogView progressDialogView;
	SessionManager sessionManager;
	private Context context;

	public LeadsAdapter(Context context, List<ListLeadsItem> modelList)
	{
		this.context = context;
		this.modelList = modelList;
	}

	@NonNull
	@Override
	public LeadsViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new LeadsViewholder(LayoutInflater.from(context).inflate(R.layout.lead_recycler_cell, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull LeadsViewholder holder, int position)
	{
		ListLeadsItem model = modelList.get(position);
		holder.txtUserName.setText(model.getFirstName());
		holder.txtBudgetName.setText(model.getBudget());
		if(model.getNoOfSeats()>1)
			holder.txtLookingFor.setText(String.valueOf(model.getNoOfSeats())+" seats");
		else
			holder.txtLookingFor.setText(String.valueOf("0")+" seat");
		if(model.getLeadCost()!=null&&!model.getLeadCost().equalsIgnoreCase(""))
		holder.txtLeadCoast.setText(model.getLeadCost());
		else
		{
			holder.txtLeadCoast.setVisibility(View.GONE);
			holder.appCompatTextViewLeadCost.setVisibility(View.GONE);
		}

		holder.txtType.setText(model.getOfficeType());
		holder.txtLocation.setText(model.getLocation());
		holder.txt_date.setText(model.getDate());
		holder.txtReason.setText(model.getReason());
		holder.txtReasonStatus.setText(model.getReasonStatus());
		if(model.getMobileNo()!=null&&!model.getMobileNo().equalsIgnoreCase(""))
		holder.txtPhone.setText(model.getMobileNo());
		else
		{
			holder.txtPhone.setVisibility(View.GONE);
			holder.linearPhone.setVisibility(View.GONE);
		}
		if(model.getEmail()!=null&&!model.getEmail().equalsIgnoreCase(""))
		holder.txtEmail.setText(model.getEmail());
		else
		{
			holder.txtEmail.setVisibility(View.GONE);
			holder.linearMail.setVisibility(View.GONE);
		}
		setStatus(model.getStatus(), holder);
	}

	private void setStatus(String status, LeadsViewholder holder)
	{
		switch (status)
		{
			case ConstantVariables.LeadsConstants.KeyAccepted:
				holder.txt_status.setText(ConstantVariables.LeadsConstants.StringAccepted);
				holder.txt_status.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.accepted));
				holder.linearActions.setVisibility(View.GONE);
				holder.imgGetDetails.setVisibility(View.VISIBLE);
				holder.linearDetails.setVisibility(View.VISIBLE);
				holder.linearReason.setVisibility(View.GONE);
				holder.linearMail.setVisibility(View.VISIBLE);
				holder.linearPhone.setVisibility(View.VISIBLE);
				break;
			case ConstantVariables.LeadsConstants.KeyConverted:
				holder.txt_status.setText(ConstantVariables.LeadsConstants.StringConverted);
				holder.txt_status.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.converted));
				holder.linearActions.setVisibility(View.GONE);
				holder.imgGetDetails.setVisibility(View.GONE);
				holder.linearDetails.setVisibility(View.VISIBLE);
				holder.linearReason.setVisibility(View.GONE);
				holder.linearMail.setVisibility(View.GONE);
				holder.linearPhone.setVisibility(View.GONE);
				break;
			case ConstantVariables.LeadsConstants.KeyDiscarded:
				holder.txt_status.setText(ConstantVariables.LeadsConstants.StringDiscarded);
				holder.txt_status.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.discarded));
				holder.linearActions.setVisibility(View.GONE);
				holder.imgGetDetails.setVisibility(View.GONE);
				holder.linearDetails.setVisibility(View.GONE);
				holder.linearReason.setVisibility(View.VISIBLE);
				holder.linearMail.setVisibility(View.GONE);
				holder.linearPhone.setVisibility(View.GONE);
				break;
			case ConstantVariables.LeadsConstants.KeyPending:
				holder.txt_status.setText(ConstantVariables.LeadsConstants.StringPending);
				holder.txt_status.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pending));
				holder.linearActions.setVisibility(View.VISIBLE);
				holder.imgGetDetails.setVisibility(View.GONE);
				holder.linearDetails.setVisibility(View.VISIBLE);
				holder.linearReason.setVisibility(View.GONE);
				holder.linearMail.setVisibility(View.GONE);
				holder.linearPhone.setVisibility(View.GONE);
				break;

		}
	}

	public void setData(List<ListLeadsItem> modelList)
	{
		this.modelList.addAll(modelList);
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount()
	{
		if (modelList != null)
		{
			return modelList.size();
		}
		else
		{
			return 0;
		}
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case AcceptLeadNWIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, true))
				{
					modelList.get(Position).setStatus(ConstantVariables.LeadsConstants.KeyAccepted);
					notifyDataSetChanged();
				}
				break;
			case DismissListIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractDataForListReasons(Response);
				}
				else
				{
					Toast.makeText(context, ToastMessages.NoRejectionList, Toast.LENGTH_SHORT).show();
				}
				break;
			case RejectLeadNWIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, true))
				{
					OfficeBingDilogs.getInstance().DismissDialog();
					modelList.get(Position).setStatus(ConstantVariables.LeadsConstants.KeyDiscarded);
					notifyDataSetChanged();
				}
				break;
		}

	}

	private void ExtractDataForListReasons(String response)
	{
		try
		{
			ListReasonsResponse listFollowupResponse = new Gson().fromJson(response, ListReasonsResponse.class);
			if (listFollowupResponse.getData() != null && listFollowupResponse.getData().size() > 0)
			{
				List<ListReasonsDataItem> listDisputes = listFollowupResponse.getData();
				ReasonsStatusAdapter adapter = new ReasonsStatusAdapter(listDisputes, context, ConstantVariables.ApiValues.StringDeclineType, this, DismissListIdentifier);

				////reject leads reasons dialog
				OfficeBingDilogs.getInstance().initDialog(context, DialogType.Select, this, new OfficeBingDialogModel(R.drawable.logo_potrait, ToastMessages.StringCancelLead, ToastMessages.StringCancelMessage, adapter), DismissListIdentifier);
			}
			else
			{
				Toast.makeText(context, ToastMessages.NoRejectionList, Toast.LENGTH_SHORT).show();
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataOfFollowupList: Error Extracting list Followup" + e.getLocalizedMessage());
			Toast.makeText(context, ToastMessages.NoRejectionList, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		Toast.makeText(context, ToastMessages.ErrorOccurred, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void whatDialogSaid(Serializable serializable, boolean WhichClicked, int Identifier)
	{
		switch (Identifier)
		{
			case AcceptLeadByDialogIdentify:
				if (WhichClicked)
				{
					AcceptLead();
				}
				break;

			case DismissListIdentifier:
				if (WhichClicked)
				{
					RejectLead((ListReasonsDataItem) serializable);
				}
				break;
		}
	}

	private void getReasons()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.ListReasons, getBodyForListRejectReasons(), DismissListIdentifier, true);
	}

	@NotNull
	private JsonObject getBodyForListRejectReasons()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, modelList.get(Position).getId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringTypeKey, ConstantVariables.ApiValues.StringDeclineType);
		Log.d(TAG, "getBodyForListReject: jsonObject: " + jsonObject);
		return jsonObject;
	}

	private void AcceptLead()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.AcceptLead, getBodyForAcceptingLead(), AcceptLeadNWIdentifier, true);
	}

	private void RejectLead(ListReasonsDataItem item)
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.RejectLead, getBodyForRejectingLead(item), RejectLeadNWIdentifier, true);
	}

	private JsonObject getBodyForAcceptingLead()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, modelList.get(Position).getId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		Log.d(TAG, "getBodyForAcceptingLead: jsonObject: " + jsonObject);
		return jsonObject;
	}

	private JsonObject getBodyForRejectingLead(ListReasonsDataItem item)
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, modelList.get(Position).getId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringRejectionComment, item.getComment());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringRejectionReasonId, item.getId());
		Log.d(TAG, "getBodyForRejectingLead: jsonObject: " + jsonObject);
		return jsonObject;
	}

	class LeadsViewholder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
		AppCompatTextView txtUserName, txtBudgetName, txtLookingFor, txtLeadCoast, txtLocation, txt_date, txt_status, txtReason, txtReasonStatus, txtEmail, txtPhone, txtType,appCompatTextViewLeadCost;
		AppCompatButton btnAccept, btnDecline;
		AppCompatImageView imgGetDetails;
		LinearLayout linearActions, linearDetails, linearReason, linearMail, linearPhone;

		public LeadsViewholder(@NonNull View itemView)
		{
			super(itemView);
			txtUserName = itemView.findViewById(R.id.txt_user_name);
			appCompatTextViewLeadCost = itemView.findViewById(R.id.appCompatTextViewLeadCost);
			txtType = itemView.findViewById(R.id.txt_type);
			txtEmail = itemView.findViewById(R.id.txt_email);
			txtPhone = itemView.findViewById(R.id.txt_mob_no);
			linearMail = itemView.findViewById(R.id.ll_email);
			linearPhone = itemView.findViewById(R.id.ll_mob_no);
			txtBudgetName = itemView.findViewById(R.id.txt_budget);
			txtLeadCoast = itemView.findViewById(R.id.txt_lead_cost);
			txtLocation = itemView.findViewById(R.id.txt_location);
			txtLookingFor = itemView.findViewById(R.id.txt_looking_for);
			txtReason = itemView.findViewById(R.id.txt_reason);
			txtReasonStatus = itemView.findViewById(R.id.txt_reason_status);
			txt_date = itemView.findViewById(R.id.txt_year);
			txt_status = itemView.findViewById(R.id.txt_status);
			linearActions = itemView.findViewById(R.id.linear_actions);
			imgGetDetails = itemView.findViewById(R.id.get_details);
			linearDetails = itemView.findViewById(R.id.linear_details);
			linearReason = itemView.findViewById(R.id.linear_reason);
			progressDialogView = new ProgressDialogView(context);
			networkCaller = new NetworkCaller(context, LeadsAdapter.this);
			sessionManager = new SessionManager(context);
			btnAccept = itemView.findViewById(R.id.btn_accept);
			btnAccept.setBackground(context.getResources().getDrawable(R.drawable.button_curve_green));
			btnAccept.setText(R.string.accept);
			btnDecline = itemView.findViewById(R.id.btn_dismiss);
			btnDecline.setBackground(context.getResources().getDrawable(R.drawable.button_curve_red));
			btnDecline.setText(R.string.decline);
			btnDecline.setOnClickListener(this);
			imgGetDetails.setOnClickListener(this);
			btnAccept.setOnClickListener(this);
		}


		@Override
		public void onClick(View v)
		{
			switch (v.getId())
			{
				case R.id.btn_accept:
					Position = getAdapterPosition();
					OfficeBingDilogs.getInstance().initDialog(context, DialogType.Confirm, LeadsAdapter.this, new OfficeBingDialogModel(R.drawable.logo_potrait, ToastMessages.StringAcceptLead, ToastMessages.StringAcceptLeadMessage), AcceptLeadByDialogIdentify);
					break;
				case R.id.btn_dismiss:
					Position = getAdapterPosition();
					getReasons();
					break;
				case R.id.get_details:
					Intent intent = new Intent(context, LeadsDetailsActivity.class);
					intent.putExtra(ConstantVariables.ApiValues.StringLeadIdKey, modelList.get(getAdapterPosition()).getLeadId());
					intent.putExtra(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, modelList.get(getAdapterPosition()).getId());
					context.startActivity(intent);
					break;
			}
		}


	}

}
