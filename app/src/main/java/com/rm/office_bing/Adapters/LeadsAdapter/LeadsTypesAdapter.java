package com.rm.office_bing.Adapters.LeadsAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Communicators.SerializedPassing;
import com.rm.office_bing.Models.LeadsModels.LeadsAdapterModel;
import com.rm.office_bing.R;

import java.io.Serializable;
import java.util.List;

public class LeadsTypesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	Context context;
	SerializedPassing callBacker;
	int Identifier;
	List<LeadsAdapterModel> leadsAdapterModelList;
	int previousChecked = -1;

	public LeadsTypesAdapter(Context context, SerializedPassing callBacker, int identifier, List<LeadsAdapterModel> leadsAdapterModelList)
	{
		this.context = context;
		this.callBacker = callBacker;
		Identifier = identifier;
		this.leadsAdapterModelList = leadsAdapterModelList;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new LeadsViewHolder(LayoutInflater.from(context).inflate(R.layout.radio_button, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{

		if (holder instanceof LeadsViewHolder)
		{
			LeadsViewHolder leadsViewHolder = (LeadsViewHolder) holder;
			LeadsAdapterModel model = leadsAdapterModelList.get(position);
			leadsViewHolder.radioLead.setText(model.getTitle());
			leadsViewHolder.radioLead.setChecked(model.getChecked());
			if (model.getChecked())
			{
				previousChecked = position;
			}
		}
	}

	@Override
	public int getItemCount()
	{
		if (leadsAdapterModelList != null)
		{
			return leadsAdapterModelList.size();
		}
		else
		{
			return 0;
		}
	}

	class LeadsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
		RadioButton radioLead;

		public LeadsViewHolder(@NonNull View itemView)
		{
			super(itemView);
			radioLead = itemView.findViewById(R.id.radio_lead);
			radioLead.setOnClickListener(this);
		}

		@Override
		public void onClick(View v)
		{
			final int position = getAdapterPosition();
			for (LeadsAdapterModel model : leadsAdapterModelList)
			{
				model.setChecked(false);
			}
			leadsAdapterModelList.get(position).setChecked(true);
			notifyDataSetChanged();
			callBacker.PassData((Serializable) leadsAdapterModelList, Identifier);
		}
	}
}
