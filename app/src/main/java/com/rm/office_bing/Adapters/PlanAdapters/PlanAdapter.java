package com.rm.office_bing.Adapters.PlanAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Models.Plans.ListPlans.ListPlansDataItem;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.AddMoneyHolder>
{
	private static final String TAG = "PlanAdapter";
	List<ListPlansDataItem> vModel = new ArrayList<>();
	SessionManager sessionManager;
	private Context context;

	public PlanAdapter(Context context, List<ListPlansDataItem> vModel, SessionManager sessionManager)
	{
		this.context = context;
		this.vModel = vModel;
		this.sessionManager = sessionManager;
	}

	@NonNull
	@Override
	public AddMoneyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new AddMoneyHolder(LayoutInflater.from(context).inflate(R.layout.plan_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull AddMoneyHolder holder, final int position)
	{
		final ListPlansDataItem addMoneyModel = vModel.get(position);
		holder.txtPlanName.setText(addMoneyModel.getPlanName());
		holder.txtPrice.setText("Rs. " + addMoneyModel.getDisplayAmount());
		holder.txtDesc.setText(addMoneyModel.getDescription());
	}

	@Override
	public int getItemCount()
	{
		return vModel.size();
	}

	public class AddMoneyHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
		public Button btnBuyPlan;
		TextView txtPlanName, txtPrice, txtDesc;

		public AddMoneyHolder(@NonNull View itemView)
		{
			super(itemView);
			btnBuyPlan = itemView.findViewById(R.id.btn_buy);
			txtPlanName = itemView.findViewById(R.id.txt_plan_title);
			txtDesc = itemView.findViewById(R.id.txt_desc);
			txtPrice = itemView.findViewById(R.id.plan_price);
			btnBuyPlan.setOnClickListener(this);
		}

		@Override
		public void onClick(View v)
		{
			switch (v.getId())
			{
				case R.id.btn_buy:
					/*Intent intent = new Intent(context, PaymentGateways.class);
					intent.putExtra(ConstantVariables.ApiValues.StringPlanId, vModel.get(getAdapterPosition()).getId());
					context.startActivity(intent);*/
					break;
			}
		}
	}
}
