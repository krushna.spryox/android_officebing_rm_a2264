package com.rm.office_bing.Adapters.SpacesAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Models.SpacesModel.ListSpaces.ListSpacesItem;
import com.rm.office_bing.R;

import java.util.List;

public class ListMySpacesAdapter extends RecyclerView.Adapter<ListMySpacesAdapter.ListMySpaceViewHolder>
{
	private static final String TAG = "ListMySpacesAdapter";
	Context context;
	List<ListSpacesItem> spacesList;
	boolean isRequestToShow = false;

	public ListMySpacesAdapter(Context context, List<ListSpacesItem> spacesList, boolean isRequestToShow)
	{
		this.context = context;
		this.spacesList = spacesList;
		this.isRequestToShow = isRequestToShow;
	}

	public ListMySpacesAdapter(Context context, List<ListSpacesItem> spacesList)
	{
		this.context = context;
		this.spacesList = spacesList;
		this.isRequestToShow = isRequestToShow;
	}

	@NonNull
	@Override
	public ListMySpaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new ListMySpaceViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_cell_my_spaces, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull ListMySpaceViewHolder holder, int position)
	{
		ListSpacesItem Model = spacesList.get(position);
		if (isRequestToShow)
		{
			if (Model.getIsRequestedToChange())
			{
				holder.txtSpaceName.setText(Model.getName());
				holder.txtLocationName.setText(Model.getLocation());
			}
		}
		else
		{
			holder.txtSpaceName.setText(Model.getName());
			holder.txtLocationName.setText(Model.getLocation());
		}
	}

	@Override
	public int getItemCount()
	{
		if (spacesList != null)
		{
			return spacesList.size();
		}
		else
		{
			return 0;
		}
	}

	private boolean isValidData(ListSpacesItem item)
	{
		return item != null;
	}

	class ListMySpaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
	{
		AppCompatTextView txtSpaceName, txtLocationName;
		ConstraintLayout constraintVisitSite;

		public ListMySpaceViewHolder(@NonNull View itemView)
		{
			super(itemView);
			txtSpaceName = itemView.findViewById(R.id.txt_space_name);
			txtLocationName = itemView.findViewById(R.id.txt_location_name);
			constraintVisitSite = itemView.findViewById(R.id.constraint_visit_site);

			constraintVisitSite.setOnClickListener(this);
		}

		@Override
		public void onClick(View v)
		{

			switch (v.getId())
			{
				case R.id.constraint_visit_site:
					final int Position = getAdapterPosition();
					final ListSpacesItem item = spacesList.get(Position);
					if (isValidData(item))
					{
						//dialogWebView.initDialog(context,URL);
						//Intent intent = new Intent(context, MySpaceDetails.class);
						//intent.putExtra(ConstantVariables.ApiValues.StringSpaceId, item);
						//context.startActivity(intent);
					}
					break;
			}
		}
	}


}
