package com.rm.office_bing.Adapters.WalletAdapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Activities.LeadsDetailsActivity;
import com.rm.office_bing.Models.WalletModel.ListRechargeHistory.ListRechargeDataItem;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class RechargeHistoryAdapter extends RecyclerView.Adapter<RechargeHistoryAdapter.AddMoneyHolder>
{
	List<ListRechargeDataItem> moneyLists = new ArrayList<>();
	SessionManager sessionManager;
	private Context context;

	public RechargeHistoryAdapter(List<ListRechargeDataItem> moneyLists, Context context, SessionManager sessionManager)
	{
		this.moneyLists = moneyLists;
		this.context = context;
		this.sessionManager = sessionManager;
	}

	@NonNull
	@Override
	public AddMoneyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return new AddMoneyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell_history, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull AddMoneyHolder holder, final int position)
	{
		ListRechargeDataItem model = moneyLists.get(position);
		if (model.getWalletType().equals(ConstantVariables.CommonValues.StringPurchased))
		{
			holder.txtMoney.setText("Rs. -" + model.getAmount());
			holder.txtMoney.setTextColor(context.getResources().getColor(R.color.red_900));
			holder.txtTypeLeadPlan.setText("Lead Type");
			holder.txtTitle.setText(model.getLeadName());
			holder.imgDetail.setVisibility(View.VISIBLE);
		}
		else
		{
			holder.imgDetail.setVisibility(View.GONE);
			holder.txtMoney.setText("Rs. +" + model.getAmount());
			holder.txtMoney.setTextColor(context.getResources().getColor(R.color.green_700));
			holder.txtTypeLeadPlan.setText("Plan Name");
			holder.txtTitle.setText(model.getPlanName());
		}
		holder.txtDate.setText(model.getDate());
	}

	@Override
	public int getItemCount()
	{
		return moneyLists.size();
	}

	public class AddMoneyHolder extends RecyclerView.ViewHolder
	{
		public Button btn_amount;
		AppCompatTextView txtTitle, txtMoney, txtDate, txtTypeLeadPlan;
		AppCompatImageView imgDetail;
		CardView cardDetail;

		public AddMoneyHolder(@NonNull View itemView)
		{
			super(itemView);
			txtTitle = itemView.findViewById(R.id.txt_order_id);
			txtMoney = itemView.findViewById(R.id.txt__money);
			txtDate = itemView.findViewById(R.id.txt_date);
			txtTypeLeadPlan = itemView.findViewById(R.id.txt_type_plan_lead);
			imgDetail = itemView.findViewById(R.id.img_detail);
			cardDetail = itemView.findViewById(R.id.card_detail);
			cardDetail.setOnClickListener(v ->
			{
				final int position = getAdapterPosition();
				if (imgDetail.getVisibility() == View.VISIBLE)
				{
					Intent intent = new Intent(context, LeadsDetailsActivity.class);
					intent.putExtra(ConstantVariables.ApiValues.StringLeadIdKey, moneyLists.get(position).getLeadId());
					intent.putExtra(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, moneyLists.get(position).getSpacePartnerLeadId());
					context.startActivity(intent);
				}
			});
		}
	}
}
