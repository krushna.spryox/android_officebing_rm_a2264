package com.rm.office_bing.Adapters.WalletAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rm.office_bing.Models.WalletModel.ListTransactionHistory.ListTransactionDataItem;
import com.rm.office_bing.R;
import com.rm.office_bing.databinding.RecyclerCellTransactionHistoryBinding;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>
{
	private Context context;
	private List<ListTransactionDataItem> listTransactions;
	private RecyclerCellTransactionHistoryBinding binding;

	public TransactionAdapter(Context context, List<ListTransactionDataItem> listTransactions)
	{
		this.context = context;
		this.listTransactions = listTransactions;
	}

	@NonNull
	@Override
	public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		View layoutInflator = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell_transaction_history, parent, false);
		binding = RecyclerCellTransactionHistoryBinding.bind(layoutInflator);
		return new TransactionViewHolder(layoutInflator);
	}

	@Override
	public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position)
	{
		ListTransactionDataItem model = listTransactions.get(position);
		binding.txtOrderId.setText(model.getOrderId());
		binding.txtTransactionNo.setText(model.getTransactionId());
		binding.txtRecieptNo.setText(model.getPaymentReceipt());
		binding.txtPaymentType.setText(model.getPaymentMode());
		binding.txtPayStatus.setText(model.getPaymentStatus());
		binding.txtDate.setText(model.getDate());
		binding.txtAmount.setText("Rs. " + model.getAmount());

	}

	@Override
	public int getItemCount()
	{
		return listTransactions.size();
	}

	class TransactionViewHolder extends RecyclerView.ViewHolder
	{
		TransactionViewHolder(@NonNull View itemView)
		{
			super(itemView);
		}
	}
}
