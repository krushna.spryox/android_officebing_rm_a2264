package com.rm.office_bing.Adapters.WalletAdapters;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.rm.office_bing.Models.WalletModel.WalletHistoryModel;

import java.util.ArrayList;
import java.util.List;

public class WalletHistoryViewpagerAdapter extends FragmentStatePagerAdapter
{
	private static final String TAG = "WalletHistoryViewpager";
	private List<WalletHistoryModel> model = new ArrayList<>();

	private WalletHistoryViewpagerAdapter(@NonNull FragmentManager fm, int behavior)
	{
		super(fm, behavior);
	}

	public WalletHistoryViewpagerAdapter(@NonNull FragmentManager fm, int behavior, List<WalletHistoryModel> model)
	{
		this(fm, behavior);
		this.model = model;
	}

	@NonNull
	@Override
	public Fragment getItem(int position)
	{
		Log.d(TAG, "getItem: " + position);
		return model.get(position).getFragment();
	}

	@Override
	public int getCount()
	{
		return model.size();
	}

	@Nullable
	@Override
	public CharSequence getPageTitle(int position)
	{
		return model.get(position).getTitle();
	}

}
