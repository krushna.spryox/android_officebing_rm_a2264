package com.rm.office_bing.Brodcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.ParentActivity.InternetStatus;


public class NetworkConnectionBrods extends BroadcastReceiver
{
	static final NetworkConnectionBrods brods = new NetworkConnectionBrods();
	NetworkCallbacker callbacker = null;
	Context context = null;

	public NetworkConnectionBrods()
	{
	}

	public static NetworkConnectionBrods getInstance()
	{
		return brods;
	}

	public void setData(NetworkCallbacker callbacker, Context context)
	{
		if (this.context != null)
		{
			//this.context.unregisterReceiver(this);
			//this.callbacker=null;
		}
		this.callbacker = callbacker;
		this.context = context;
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
		this.context.registerReceiver(this, filter);
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		if (callbacker != null)
		{
			callbacker.StateChange(InternetStatus.isNetwork(context.getApplicationContext()));
		}
	}
}
