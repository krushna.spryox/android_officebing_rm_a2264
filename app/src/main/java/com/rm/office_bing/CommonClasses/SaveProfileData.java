package com.rm.office_bing.CommonClasses;

import android.util.Log;

import com.google.gson.Gson;
import com.rm.office_bing.Models.ProfileModels.FetchProfile.FetchProfileResponse;
import com.rm.office_bing.Utils.SessionManager;

public class SaveProfileData
{
	String TAG="SaveProfileData";
	final static SaveProfileData saveProfileData=new SaveProfileData();

	private SaveProfileData() { }

	public static SaveProfileData init()
	{
		return saveProfileData;
	}
	public void startSaving(String Response, SessionManager sessionManager)
	{
		try
		{
			FetchProfileResponse fetchProfileResponse=new Gson().fromJson(Response,FetchProfileResponse.class);
			if(fetchProfileResponse.getData()==null)
			{
				Log.d(TAG, "startSaving:Profile Data was null");
				return;
			}
			sessionManager.setUserID(String.valueOf(fetchProfileResponse.getData().getId()));
			sessionManager.setPlanId(String.valueOf(fetchProfileResponse.getData().getPlanId()));
			sessionManager.setPlanName(fetchProfileResponse.getData().getCurrentPlan());
			sessionManager.setGSTNo(fetchProfileResponse.getData().getGstNo());
			sessionManager.setCompanyName(fetchProfileResponse.getData().getCompany());
			sessionManager.setMobileNo(fetchProfileResponse.getData().getMobile());
			sessionManager.setFirstName(fetchProfileResponse.getData().getName());
			if(fetchProfileResponse.getData().getName()!=null&&!fetchProfileResponse.getData().getName().equalsIgnoreCase(""))
			sessionManager.setInitial(String.valueOf(fetchProfileResponse.getData().getName().charAt(0)));

			sessionManager.setWalletAmt(fetchProfileResponse.getData().getWalletAmount()==null?"0":fetchProfileResponse.getData().getWalletAmount());
		}
		catch (Exception e)
		{
			Log.d(TAG, "startSaving:Error Extracting profile data "+e.getMessage());
		}

	}
}
