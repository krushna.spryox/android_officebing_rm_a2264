package com.rm.office_bing.Communicators;

import java.io.Serializable;

public interface DialogCommunicator
{
	void whatDialogSaid(Serializable serializable, boolean WhichClicked, int Identifier);
}
