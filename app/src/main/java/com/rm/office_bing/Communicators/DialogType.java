package com.rm.office_bing.Communicators;

public enum DialogType
{
	Info, Confirm, Edit, Select
}
