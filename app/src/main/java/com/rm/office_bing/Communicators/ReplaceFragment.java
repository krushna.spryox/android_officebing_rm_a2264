package com.rm.office_bing.Communicators;

import androidx.fragment.app.Fragment;

public interface ReplaceFragment
{
	void replace(Fragment fragment);
}
