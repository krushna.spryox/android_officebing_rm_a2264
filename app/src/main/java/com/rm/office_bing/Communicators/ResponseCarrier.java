package com.rm.office_bing.Communicators;

public interface ResponseCarrier
{
	void Success(String Response, int Identifier);

	void Error(Throwable Response, int Identifier);
}
