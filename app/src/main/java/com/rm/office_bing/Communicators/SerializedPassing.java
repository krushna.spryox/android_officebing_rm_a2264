package com.rm.office_bing.Communicators;

import java.io.Serializable;

public interface SerializedPassing
{
	void PassData(Serializable serializable, int Identifier);
}
