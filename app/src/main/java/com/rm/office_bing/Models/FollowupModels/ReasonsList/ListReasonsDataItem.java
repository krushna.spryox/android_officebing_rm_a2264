package com.rm.office_bing.Models.FollowupModels.ReasonsList;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListReasonsDataItem implements Serializable
{
	String Comment;
	private boolean status = false;
	@SerializedName("name")
	private String name;
	@SerializedName("id")
	private int id;
	@SerializedName("code_name")
	private String code_name;

	public String getCode_name()
	{
		return code_name;
	}

	public void setCode_name(String code_name)
	{
		this.code_name = code_name;
	}

	public String getComment()
	{
		return Comment;
	}

	public void setComment(String comment)
	{
		Comment = comment;
	}

	public boolean isStatus()
	{
		return status;
	}

	public void setStatus(boolean status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return
				"DataItem{" +
						"name = '" + name + '\'' +
						",id = '" + id + '\'' +
						"}";
	}
}