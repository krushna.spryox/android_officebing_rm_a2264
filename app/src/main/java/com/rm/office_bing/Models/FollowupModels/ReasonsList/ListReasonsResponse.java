package com.rm.office_bing.Models.FollowupModels.ReasonsList;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListReasonsResponse implements Serializable
{

	@SerializedName("data")
	private List<ListReasonsDataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public List<ListReasonsDataItem> getData()
	{
		return data;
	}

	public void setData(List<ListReasonsDataItem> data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}