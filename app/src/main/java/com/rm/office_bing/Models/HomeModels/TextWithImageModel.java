package com.rm.office_bing.Models.HomeModels;


import android.content.Intent;

public class TextWithImageModel
{
	private String Title;
	private int ResourceId;
	private String ImageUrl;
	private Intent intent;

	public TextWithImageModel(String title, int resourceId, String imageUrl, Intent intent)
	{
		Title = title;
		ResourceId = resourceId;
		ImageUrl = imageUrl;
		this.intent = intent;
	}

	public Intent getIntent()
	{
		return intent;
	}

	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}

	public String getTitle()
	{
		return Title;
	}

	public void setTitle(String title)
	{
		Title = title;
	}

	public int getResourceId()
	{
		return ResourceId;
	}

	public void setResourceId(int resourceId)
	{
		ResourceId = resourceId;
	}

	public String getImageUrl()
	{
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		ImageUrl = imageUrl;
	}
}
