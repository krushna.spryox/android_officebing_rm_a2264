package com.rm.office_bing.Models.LeadsModels.LeadDetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LeadDetailsDataItem implements Serializable
{

	@SerializedName("pincode")
	private int pincode;

	@SerializedName("office_type")
	private String officeType;

	@SerializedName("dispute_reason_status")
	private String disputeReasonStatus;

	@SerializedName("addedby")
	private int addedby;

	@SerializedName("space_partner_lead_id ")
	private int spacePartnerLeadId;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("seats")
	private int seats;

	@SerializedName("location_id")
	private int locationId;

	@SerializedName("leads_sources")
	private String leadsSources;

	@SerializedName("city_name")
	private String cityName;

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("leads_status")
	private String leadsStatus;

	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("conversation")
	private String conversation;

	@SerializedName("budget")
	private String budget;

	@SerializedName("status")
	private String status;
	@SerializedName("tentative_date")
	private String TentativeDate;

	public String getTentativeDate()
	{
		return TentativeDate;
	}

	public void setTentativeDate(String tentativeDate)
	{
		TentativeDate = tentativeDate;
	}

	public int getPincode()
	{
		return pincode;
	}

	public void setPincode(int pincode)
	{
		this.pincode = pincode;
	}

	public String getOfficeType()
	{
		return officeType;
	}

	public void setOfficeType(String officeType)
	{
		this.officeType = officeType;
	}

	public String getDisputeReasonStatus()
	{
		return disputeReasonStatus;
	}

	public void setDisputeReasonStatus(String disputeReasonStatus)
	{
		this.disputeReasonStatus = disputeReasonStatus;
	}

	public int getAddedby()
	{
		return addedby;
	}

	public void setAddedby(int addedby)
	{
		this.addedby = addedby;
	}

	public int getSpacePartnerLeadId()
	{
		return spacePartnerLeadId;
	}

	public void setSpacePartnerLeadId(int spacePartnerLeadId)
	{
		this.spacePartnerLeadId = spacePartnerLeadId;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(String createdAt)
	{
		this.createdAt = createdAt;
	}

	public int getSeats()
	{
		return seats;
	}

	public void setSeats(int seats)
	{
		this.seats = seats;
	}

	public int getLocationId()
	{
		return locationId;
	}

	public void setLocationId(int locationId)
	{
		this.locationId = locationId;
	}

	public String getLeadsSources()
	{
		return leadsSources;
	}

	public void setLeadsSources(String leadsSources)
	{
		this.leadsSources = leadsSources;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public Object getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(Object updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public String getLeadsStatus()
	{
		return leadsStatus;
	}

	public void setLeadsStatus(String leadsStatus)
	{
		this.leadsStatus = leadsStatus;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getConversation()
	{
		return conversation;
	}

	public void setConversation(String conversation)
	{
		this.conversation = conversation;
	}

	public String getBudget()
	{
		return budget;
	}

	public void setBudget(String budget)
	{
		this.budget = budget;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return
				"DataItem{" +
						"pincode = '" + pincode + '\'' +
						",office_type = '" + officeType + '\'' +
						",dispute_reason_status = '" + disputeReasonStatus + '\'' +
						",addedby = '" + addedby + '\'' +
						",space_partner_lead_id  = '" + spacePartnerLeadId + '\'' +
						",mobile = '" + mobile + '\'' +
						",last_name = '" + lastName + '\'' +
						",created_at = '" + createdAt + '\'' +
						",seats = '" + seats + '\'' +
						",location_id = '" + locationId + '\'' +
						",leads_sources = '" + leadsSources + '\'' +
						",city_name = '" + cityName + '\'' +
						",updated_at = '" + updatedAt + '\'' +
						",leads_status = '" + leadsStatus + '\'' +
						",id = '" + id + '\'' +
						",first_name = '" + firstName + '\'' +
						",email = '" + email + '\'' +
						",conversation = '" + conversation + '\'' +
						",budget = '" + budget + '\'' +
						",status = '" + status + '\'' +
						"}";
	}
}