package com.rm.office_bing.Models.LeadsModels;

public class LeadsAdapterModel
{
	private String Title;
	private String Key;
	private Boolean isChecked;

	public LeadsAdapterModel(String title, Boolean isChecked, String key)
	{
		Title = title;
		Key = key;
		this.isChecked = isChecked;
	}

	public String getKey()
	{
		return Key;
	}

	public void setKey(String key)
	{
		Key = key;
	}

	public String getTitle()
	{
		return Title;
	}

	public void setTitle(String title)
	{
		Title = title;
	}

	public Boolean getChecked()
	{
		return isChecked;
	}

	public void setChecked(Boolean checked)
	{
		isChecked = checked;
	}
}
