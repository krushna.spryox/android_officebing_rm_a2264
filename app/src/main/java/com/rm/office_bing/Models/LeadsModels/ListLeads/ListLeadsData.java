package com.rm.office_bing.Models.LeadsModels.ListLeads;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListLeadsData implements Serializable
{

	@SerializedName("leads")
	private List<ListLeadsItem> leads;

	public List<ListLeadsItem> getLeads()
	{
		return leads;
	}

	public void setLeads(List<ListLeadsItem> leads)
	{
		this.leads = leads;
	}

	@Override
	public String toString()
	{
		return
				"Data{" +
						"leads = '" + leads + '\'' +
						"}";
	}
}