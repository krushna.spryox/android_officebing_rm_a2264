package com.rm.office_bing.Models.LeadsModels.ListLeads;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.KeyAccepted;

public class ListLeadsItem implements Serializable
{

	@SerializedName("lead_cost")
	private String leadCost;

	@SerializedName("date")
	private String date;

	@SerializedName("no_of_seats ")
	private int noOfSeats;

	@SerializedName("reason")
	private String reason;
	@SerializedName("decline_reason_status")
	private String reasonStatus;

	@SerializedName("office_type")
	private String officeType;

	@SerializedName("location")
	private String location;

	@SerializedName("id")
	private int id;

	@SerializedName("name")
	private String firstName;

	@SerializedName("lead_id")
	private int leadId;

	@SerializedName("budget")
	private String budget;

	@SerializedName("status")
	private String status=KeyAccepted;
	@SerializedName("mobile")
	private String mobileNo;
	@SerializedName("email")
	private String email;

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getLeadCost()
	{
		return leadCost;
	}

	public void setLeadCost(String leadCost)
	{
		this.leadCost = leadCost;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public int getNoOfSeats()
	{
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats)
	{
		this.noOfSeats = noOfSeats;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public String getOfficeType()
	{
		return officeType;
	}

	public void setOfficeType(String officeType)
	{
		this.officeType = officeType;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public int getLeadId()
	{
		return leadId;
	}

	public void setLeadId(int leadId)
	{
		this.leadId = leadId;
	}

	public String getBudget()
	{
		return budget;
	}

	public void setBudget(String budget)
	{
		this.budget = budget;
	}

	public String getReasonStatus()
	{
		return reasonStatus;
	}

	public void setReasonStatus(String reasonStatus)
	{
		this.reasonStatus = reasonStatus;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return
				"LeadsItem{" +
						"lead_cost = '" + leadCost + '\'' +
						",date = '" + date + '\'' +
						",no_of_seats  = '" + noOfSeats + '\'' +
						",reason = '" + reason + '\'' +
						",office_type = '" + officeType + '\'' +
						",location = '" + location + '\'' +
						",id = '" + id + '\'' +
						",first_name = '" + firstName + '\'' +
						",lead_id = '" + leadId + '\'' +
						",budget = '" + budget + '\'' +
						",status = '" + status + '\'' +
						"}";
	}
}