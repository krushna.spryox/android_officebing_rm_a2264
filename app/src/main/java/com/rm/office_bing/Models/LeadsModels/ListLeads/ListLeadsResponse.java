package com.rm.office_bing.Models.LeadsModels.ListLeads;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListLeadsResponse implements Serializable
{

	@SerializedName("data")
	private ListLeadsData data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public ListLeadsData getData()
	{
		return data;
	}

	public void setData(ListLeadsData data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}