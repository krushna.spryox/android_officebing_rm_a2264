package com.rm.office_bing.Models;

import androidx.recyclerview.widget.RecyclerView;

public class OfficeBingDialogModel
{
	int Icon = -1;
	String imgPath;
	String Title;
	String Message;
	RecyclerView.Adapter adapter;

	public OfficeBingDialogModel(String imgPath, String title, String message)
	{
		this.imgPath = imgPath;
		Title = title;
		Message = message;
	}

	public OfficeBingDialogModel(String imgPath, String title, String message, RecyclerView.Adapter list)
	{
		this.imgPath = imgPath;
		Title = title;
		Message = message;
		this.adapter = list;
	}


	public OfficeBingDialogModel(int icon, String title, String message)
	{
		Icon = icon;
		Title = title;
		Message = message;

	}

	public OfficeBingDialogModel(int icon, String title, String message, RecyclerView.Adapter list)
	{
		Icon = icon;
		Title = title;
		Message = message;
		this.adapter = list;
	}

	public int getIcon()
	{
		return Icon;
	}

	public void setIcon(int icon)
	{
		Icon = icon;
	}

	public String getImgPath()
	{
		return imgPath;
	}

	public void setImgPath(String imgPath)
	{
		this.imgPath = imgPath;
	}

	public String getTitle()
	{
		return Title;
	}

	public void setTitle(String title)
	{
		Title = title;
	}

	public String getMessage()
	{
		return Message;
	}

	public void setMessage(String message)
	{
		Message = message;
	}

	public RecyclerView.Adapter getAdapter()
	{
		return adapter;
	}

	public void setAdapter(RecyclerView.Adapter adapter)
	{
		this.adapter = adapter;
	}
}
