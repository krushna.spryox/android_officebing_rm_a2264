package com.rm.office_bing.Models.PaymentModels.GetOrderDetailsModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetOrderDetailsData implements Serializable
{

	@SerializedName("amount")
	private String amount;

	@SerializedName("receipt")
	private String receipt;

	@SerializedName("id")
	private int id;

	@SerializedName("order_id")
	private String orderId;

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getReceipt()
	{
		return receipt;
	}

	public void setReceipt(String receipt)
	{
		this.receipt = receipt;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}

	@Override
	public String toString()
	{
		return
				"Data{" +
						"amount = '" + amount + '\'' +
						",receipt = '" + receipt + '\'' +
						",id = '" + id + '\'' +
						",order_id = '" + orderId + '\'' +
						"}";
	}
}