package com.rm.office_bing.Models.Plans.ListPlans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListPlansDataItem implements Serializable
{

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private String Id;

	@SerializedName("display_amount")
	private String displayAmount;

	@SerializedName("plan_name")
	private String planName;

	@SerializedName("wallet_amount")
	private String walletAmount;

	public String getDescription()
	{
		return description;
	}

	public String getDisplayAmount()
	{
		return displayAmount;
	}

	public String getPlanName()
	{
		return planName;
	}

	public String getWalletAmount()
	{
		return walletAmount;
	}

	public String getId()
	{
		return Id;
	}

	public void setId(String id)
	{
		Id = id;
	}
}