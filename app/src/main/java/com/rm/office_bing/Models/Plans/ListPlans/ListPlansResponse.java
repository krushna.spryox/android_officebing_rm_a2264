package com.rm.office_bing.Models.Plans.ListPlans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListPlansResponse implements Serializable
{

	@SerializedName("data")
	private List<ListPlansDataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public List<ListPlansDataItem> getData()
	{
		return data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public String getMessage()
	{
		return message;
	}
}