package com.rm.office_bing.Models.Plans;

public class PlansModel
{

	String plan_title;
	String plan_image;
	String price;
	String description;

	public PlansModel(String plan_title, String plan_image, String price, String description)
	{
		this.plan_title = plan_title;
		this.plan_image = plan_image;
		this.price = price;
		this.description = description;
	}

	public String getPlan_title()
	{
		return plan_title;
	}

	public void setPlan_title(String plan_title)
	{
		this.plan_title = plan_title;
	}

	public String getPlan_image()
	{
		return plan_image;
	}

	public void setPlan_image(String plan_image)
	{
		this.plan_image = plan_image;
	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
