package com.rm.office_bing.Models.ProfileModels.FetchProfile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FetchProfileData implements Serializable
{

	@SerializedName("is_active")
	private int isActive;

	@SerializedName("current_plan")
	private String currentPlan;

	@SerializedName("addedby")
	private Object addedby;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("gst_no")
	private String gstNo;

	@SerializedName("name")
	private String name;

	@SerializedName("fcm_token")
	private String fcmToken;

	@SerializedName("company")
	private String company;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("plan_id")
	private int planId;

	@SerializedName("wallet_amount")
	private String walletAmount;

	public int getIsActive()
	{
		return isActive;
	}

	public void setIsActive(int isActive)
	{
		this.isActive = isActive;
	}

	public String getCurrentPlan()
	{
		return currentPlan;
	}

	public void setCurrentPlan(String currentPlan)
	{
		this.currentPlan = currentPlan;
	}

	public Object getAddedby()
	{
		return addedby;
	}

	public void setAddedby(Object addedby)
	{
		this.addedby = addedby;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(String createdAt)
	{
		this.createdAt = createdAt;
	}

	public String getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public String getGstNo()
	{
		return gstNo;
	}

	public void setGstNo(String gstNo)
	{
		this.gstNo = gstNo;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getFcmToken()
	{
		return fcmToken;
	}

	public void setFcmToken(String fcmToken)
	{
		this.fcmToken = fcmToken;
	}

	public String getCompany()
	{
		return company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public int getPlanId()
	{
		return planId;
	}

	public void setPlanId(int planId)
	{
		this.planId = planId;
	}

	public String getWalletAmount()
	{
		return walletAmount;
	}

	public void setWalletAmount(String walletAmount)
	{
		this.walletAmount = walletAmount;
	}

	@Override
	public String toString()
	{
		return
				"Data{" +
						"is_active = '" + isActive + '\'' +
						",current_plan = '" + currentPlan + '\'' +
						",addedby = '" + addedby + '\'' +
						",mobile = '" + mobile + '\'' +
						",created_at = '" + createdAt + '\'' +
						",updated_at = '" + updatedAt + '\'' +
						",gst_no = '" + gstNo + '\'' +
						",name = '" + name + '\'' +
						",fcm_token = '" + fcmToken + '\'' +
						",company = '" + company + '\'' +
						",id = '" + id + '\'' +
						",email = '" + email + '\'' +
						",plan_id = '" + planId + '\'' +
						",wallet_amount = '" + walletAmount + '\'' +
						"}";
	}
}