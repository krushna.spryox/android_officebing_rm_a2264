package com.rm.office_bing.Models.ProfileModels.FetchProfile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FetchProfileResponse implements Serializable
{

	@SerializedName("data")
	private FetchProfileData data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public FetchProfileData getData()
	{
		return data;
	}

	public void setData(FetchProfileData data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}