package com.rm.office_bing.Models.SpacesModel.ListSpaces;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class ListSpacesData implements Serializable
{

	@SerializedName("spaces")
	private List<ListSpacesItem> spaces;

	public List<ListSpacesItem> getSpaces()
	{
		return spaces;
	}

	public void setSpaces(List<ListSpacesItem> spaces)
	{
		this.spaces = spaces;
	}

	@NotNull
	@Override
	public String toString()
	{
		return
				"Data{" +
						"spaces = '" + spaces + '\'' +
						"}";
	}
}