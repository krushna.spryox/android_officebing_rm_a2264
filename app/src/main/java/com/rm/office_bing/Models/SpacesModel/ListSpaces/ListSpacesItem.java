package com.rm.office_bing.Models.SpacesModel.ListSpaces;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class ListSpacesItem implements Serializable
{

	@SerializedName("name")
	private String name;

	@SerializedName("location")
	private String location;

	@SerializedName("url")
	private String url;

	@SerializedName("office_type")
	private String officeType;

	@SerializedName("is_listed")
	private int isListed;

	@SerializedName("request_to_change")
	private int isRequestedToChange;

	public String getOfficeType()
	{
		return officeType;
	}

	public void setOfficeType(String officeType)
	{
		this.officeType = officeType;
	}

	public boolean getIsListed()
	{
		return getBoolean(isListed);
	}

	public void setIsListed(int isListed)
	{
		this.isListed = isListed;
	}

	public boolean getIsRequestedToChange()
	{
		return getBoolean(isRequestedToChange);
	}

	public void setIsRequestedToChange(int isRequestedToChange)
	{
		this.isRequestedToChange = isRequestedToChange;
	}

	private boolean getBoolean(int value)
	{
		return value == 1;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	@NotNull
	@Override
	public String toString()
	{
		return
				"SpacesItem{" +
						"name = '" + name + '\'' +
						",location = '" + location + '\'' +
						",url = '" + url + '\'' +
						"}";
	}
}