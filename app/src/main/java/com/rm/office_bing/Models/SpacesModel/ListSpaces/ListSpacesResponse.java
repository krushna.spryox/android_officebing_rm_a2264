package com.rm.office_bing.Models.SpacesModel.ListSpaces;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class ListSpacesResponse implements Serializable
{

	@SerializedName("data")
	private ListSpacesData data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public ListSpacesData getData()
	{
		return data;
	}

	public void setData(ListSpacesData data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@NotNull
	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}