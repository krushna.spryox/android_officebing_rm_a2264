package com.rm.office_bing.Models.TimelineModels.ListTimeline;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListTimelineDataItem implements Serializable
{

	@SerializedName("date")
	private String date;

	@SerializedName("followup_status")
	private String followupStatus;

	@SerializedName("space_partner_lead_id")
	private int spacePartnerLeadId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("addedby")
	private int addedby;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("followup_status_id")
	private int followupStatusId;

	@SerializedName("comment")
	private String comment;

	@SerializedName("id")
	private int id;

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getFollowupStatus()
	{
		return followupStatus;
	}

	public void setFollowupStatus(String followupStatus)
	{
		this.followupStatus = followupStatus;
	}

	public int getSpacePartnerLeadId()
	{
		return spacePartnerLeadId;
	}

	public void setSpacePartnerLeadId(int spacePartnerLeadId)
	{
		this.spacePartnerLeadId = spacePartnerLeadId;
	}

	public String getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public int getAddedby()
	{
		return addedby;
	}

	public void setAddedby(int addedby)
	{
		this.addedby = addedby;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(String createdAt)
	{
		this.createdAt = createdAt;
	}

	public int getFollowupStatusId()
	{
		return followupStatusId;
	}

	public void setFollowupStatusId(int followupStatusId)
	{
		this.followupStatusId = followupStatusId;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return
				"DataItem{" +
						"date = '" + date + '\'' +
						",followup_status = '" + followupStatus + '\'' +
						",space_partner_lead_id = '" + spacePartnerLeadId + '\'' +
						",updated_at = '" + updatedAt + '\'' +
						",addedby = '" + addedby + '\'' +
						",created_at = '" + createdAt + '\'' +
						",followup_status_id = '" + followupStatusId + '\'' +
						",comment = '" + comment + '\'' +
						",id = '" + id + '\'' +
						"}";
	}
}