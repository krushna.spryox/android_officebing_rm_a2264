package com.rm.office_bing.Models.TimelineModels.ListTimeline;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListTimelineResponse implements Serializable
{

	@SerializedName("data")
	private List<ListTimelineDataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public List<ListTimelineDataItem> getData()
	{
		return data;
	}

	public void setData(List<ListTimelineDataItem> data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}