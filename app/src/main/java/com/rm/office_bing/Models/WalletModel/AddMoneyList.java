package com.rm.office_bing.Models.WalletModel;

public class AddMoneyList
{
	String price;

	public AddMoneyList(String price)
	{
		this.price = price;
	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}
}
