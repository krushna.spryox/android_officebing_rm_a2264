package com.rm.office_bing.Models.WalletModel.ListRechargeHistory;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListRechargeDataItem implements Serializable
{

	@SerializedName("date")
	private String date;

	@SerializedName("amount")
	private String amount;

	@SerializedName("space_partner_lead_id")
	private int spacePartnerLeadId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("addedby")
	private int addedby;

	@SerializedName("wallet_type")
	private String walletType;

	@SerializedName("action")
	private String action;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("plan_name")
	private String planName;
	@SerializedName("lead_name")
	private String leadName;

	@SerializedName("id")
	private int id;

	@SerializedName("space_partner_id")
	private int spacePartnerId;
	@SerializedName("lead_id")
	private int leadId;

	public String getPlanName()
	{
		return planName;
	}

	public void setPlanName(String planName)
	{
		this.planName = planName;
	}

	public int getLeadId()
	{
		return leadId;
	}

	public void setLeadId(int leadId)
	{
		this.leadId = leadId;
	}

	public String getLeadName()
	{
		return leadName;
	}

	public void setLeadName(String leadName)
	{
		this.leadName = leadName;
	}

	public String getDate()
	{
		return date;
	}

	public String getAmount()
	{
		return amount;
	}

	public int getSpacePartnerLeadId()
	{
		return spacePartnerLeadId;
	}

	public String getUpdatedAt()
	{
		return updatedAt;
	}

	public int getAddedby()
	{
		return addedby;
	}

	public String getWalletType()
	{
		return walletType;
	}

	public String getAction()
	{
		return action;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public int getId()
	{
		return id;
	}

	public int getSpacePartnerId()
	{
		return spacePartnerId;
	}
}