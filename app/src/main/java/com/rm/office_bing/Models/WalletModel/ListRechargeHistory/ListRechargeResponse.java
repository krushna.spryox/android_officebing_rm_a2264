package com.rm.office_bing.Models.WalletModel.ListRechargeHistory;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListRechargeResponse implements Serializable
{

	@SerializedName("data")
	private List<ListRechargeDataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public List<ListRechargeDataItem> getData()
	{
		return data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public String getMessage()
	{
		return message;
	}
}