package com.rm.office_bing.Models.WalletModel.ListTransactionHistory;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListTransactionDataItem implements Serializable
{

	@SerializedName("transaction_id")
	private String transactionId;

	@SerializedName("date")
	private String date;

	@SerializedName("payment_mode")
	private String paymentMode;

	@SerializedName("amount")
	private String amount;

	@SerializedName("addedby")
	private int addedby;

	@SerializedName("payment_status")
	private String paymentStatus;

	@SerializedName("payment_receipt")
	private String paymentReceipt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("space_partner_id")
	private int spacePartnerId;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("plan_id")
	private int planId;

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public int getAddedby()
	{
		return addedby;
	}

	public void setAddedby(int addedby)
	{
		this.addedby = addedby;
	}

	public String getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentReceipt()
	{
		return paymentReceipt;
	}

	public void setPaymentReceipt(String paymentReceipt)
	{
		this.paymentReceipt = paymentReceipt;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(String createdAt)
	{
		this.createdAt = createdAt;
	}

	public String getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getSpacePartnerId()
	{
		return spacePartnerId;
	}

	public void setSpacePartnerId(int spacePartnerId)
	{
		this.spacePartnerId = spacePartnerId;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}

	public int getPlanId()
	{
		return planId;
	}

	public void setPlanId(int planId)
	{
		this.planId = planId;
	}

	@Override
	public String toString()
	{
		return
				"DataItem{" +
						"transaction_id = '" + transactionId + '\'' +
						",date = '" + date + '\'' +
						",payment_mode = '" + paymentMode + '\'' +
						",amount = '" + amount + '\'' +
						",addedby = '" + addedby + '\'' +
						",payment_status = '" + paymentStatus + '\'' +
						",payment_receipt = '" + paymentReceipt + '\'' +
						",created_at = '" + createdAt + '\'' +
						",updated_at = '" + updatedAt + '\'' +
						",id = '" + id + '\'' +
						",space_partner_id = '" + spacePartnerId + '\'' +
						",order_id = '" + orderId + '\'' +
						",plan_id = '" + planId + '\'' +
						"}";
	}
}