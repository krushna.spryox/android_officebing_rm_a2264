package com.rm.office_bing.Models.WalletModel.ListTransactionHistory;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ListTransactionResponse implements Serializable
{

	@SerializedName("data")
	private List<ListTransactionDataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public List<ListTransactionDataItem> getData()
	{
		return data;
	}

	public void setData(List<ListTransactionDataItem> data)
	{
		this.data = data;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return
				"Response{" +
						"data = '" + data + '\'' +
						",success = '" + success + '\'' +
						",message = '" + message + '\'' +
						"}";
	}
}