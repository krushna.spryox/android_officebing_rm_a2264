package com.rm.office_bing.Models.WalletModel;

import androidx.fragment.app.Fragment;

public class WalletHistoryModel
{
	private Fragment fragment;
	private String Title;

	public WalletHistoryModel(Fragment fragment, String title)
	{
		this.fragment = fragment;
		Title = title;
	}

	public Fragment getFragment()
	{
		return fragment;
	}

	public void setFragment(Fragment fragment)
	{
		this.fragment = fragment;
	}

	public String getTitle()
	{
		return Title;
	}

	public void setTitle(String title)
	{
		Title = title;
	}
}
