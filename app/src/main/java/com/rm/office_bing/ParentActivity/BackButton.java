package com.rm.office_bing.ParentActivity;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import static android.view.View.INVISIBLE;

public class BackButton
{
	String TAG = "BackButton";

	public BackButton(@NonNull final Activity activity, @NonNull AppCompatImageView imageView, Boolean shouldShow)
	{
		Log.d(TAG, "BackButton: ");
		if (!shouldShow)
		{
			imageView.setVisibility(INVISIBLE);
			return;
		}
		imageView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d(TAG, "callOnClick: ");

				activity.onBackPressed();
			}
		});

	}

}
