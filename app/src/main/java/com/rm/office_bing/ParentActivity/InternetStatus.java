package com.rm.office_bing.ParentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

import com.rm.office_bing.R;

import org.jetbrains.annotations.NotNull;

public class InternetStatus
{
	final static InternetStatus status = new InternetStatus();

	public static InternetStatus getInstance()
	{
		return status;
	}

	public static boolean isNetwork(Context context)
	{
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	public void isOnline(Context context, @NotNull AppCompatImageView imageView, boolean shouldShow)
	{
		if (shouldShow)
		{
			if (isNetwork(context))
			{
				imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.img_internet));
			}
			else
			{
				imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.img_no_internet));
			}

		}
		else
		{
			imageView.setVisibility(View.GONE);
		}
	}
}
