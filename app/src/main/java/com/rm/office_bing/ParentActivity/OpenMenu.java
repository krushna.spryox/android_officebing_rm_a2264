package com.rm.office_bing.ParentActivity;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.MenuRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.PopupMenu;

import com.rm.office_bing.Communicators.DialogCommunicator;
import com.rm.office_bing.Communicators.DialogType;
import com.rm.office_bing.Models.OfficeBingDialogModel;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.OfficeBingDilogs;
import com.rm.office_bing.Utils.SessionManager;

import java.io.Serializable;

public class OpenMenu implements View.OnClickListener, DialogCommunicator, PopupMenu.OnMenuItemClickListener
{
	final int LogoutIdentifier = 1001;
	SessionManager manager;
	private AppCompatImageView MenuView;
	private Context context;
	private int Menu;

	public OpenMenu(@NonNull Context context, @NonNull AppCompatImageView imgMenu, @MenuRes int menuView, Boolean shouldShow)
	{
		this.context = context;
		manager = new SessionManager(context);
		MenuView = imgMenu;
		Menu = menuView;
		if (!shouldShow)
		{
			MenuView.setVisibility(View.INVISIBLE);
			return;
		}
		MenuView.setOnClickListener(this);
	}


	@Override
	public void onClick(View v)
	{
		PopupMenu popupMenu = new PopupMenu(context, v);
		popupMenu.setOnMenuItemClickListener(this);
		popupMenu.inflate(Menu);
		popupMenu.show();
	}

	@Override
	public boolean onMenuItemClick(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.men_change_password:
				//context.startActivity(new Intent(context, ChangePasswordActivity.class));
				return true;
			case R.id.men_privacy_policy:
				Toast.makeText(context, "Redirecting To Privacy Policy", Toast.LENGTH_SHORT).show();
				return true;
			case R.id.men_help:
				Toast.makeText(context, "Calling For Help", Toast.LENGTH_SHORT).show();
				return true;
			case R.id.men_logout:
				OfficeBingDilogs.getInstance().initDialog(context, DialogType.Confirm, this, new OfficeBingDialogModel(null, context.getResources().getString(R.string.logout), context.getResources().getString(R.string.logout_information)), LogoutIdentifier);
				return true;
			default:
				return false;
		}

	}

	@Override
	public void whatDialogSaid(Serializable serializable, boolean WhichClicked, int Identifier)
	{
		switch (Identifier)
		{
			case LogoutIdentifier:
                if (WhichClicked)
                {
                    manager.logoutUser();
                }
				break;
		}
	}
}
