package com.rm.office_bing.ParentActivity;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;

import org.jetbrains.annotations.NotNull;

public class SetHeader
{
	public SetHeader(@NotNull AppCompatTextView textView, @NotNull String Header, boolean shouldShow)
	{
		if (!shouldShow)
		{
			textView.setVisibility(View.GONE);
			return;
		}
		textView.setText(Header);
	}
}
