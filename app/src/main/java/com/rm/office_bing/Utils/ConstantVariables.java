package com.rm.office_bing.Utils;

import com.rm.office_bing.R;

import java.util.concurrent.TimeUnit;

public class ConstantVariables
{
	public static class ApiValues
	{
		public static String StringRevenueKey = "revenue_generated";
		public static String StringClientWonVal = "client_won";
		public static String StringFcmKey = "fcm_token";
		public static String StringFollowupType = "followup";
		public static String StringDisputeType = "dispute";
		public static String StringDeclineType = "decline";
		public static String StringNameKey = "name";
		public static String StringEmailKey = "email";
		public static String StringMobileKey = "mobile";
		public static String StringCompanyKey = "company";
		public static String StringGstNoKey = "gst_no";
		public static String StringPassKey = "password";
		public static String StringConfPassKey = "confirm_password";

		public static String StringTypeKey = "type";
		public static String StringPageKey = "page";
		public static String StringIdKey = "id";
		public static String StringLeadIdKey = "lead_id";
		public static String StringSpacePartnerLeadIdKey = "user_lead_id";
		public static String StringFollowupStatusIdKey = "status";
		public static String StringDisputeStatusIdKey = "dispute_status_id";
		public static String StringCommentKey = "comment";
		public static String StringPlanId = "plan_id";
		public static String StringTransactionId = "transaction_id";
		public static String StringOrderId = "order_id";
		public static String StringOldPassword = "current_password";
		public static String StringNewPassword = "new_password";
		public static String StringRejectionComment = "decline_reason";
		public static String StringRejectionReasonId = "decline_reason_id";
		public static String StringSpaceId = "space_id";

	}

	public static class Headers
	{
		public static String StringXApiKey = "x-api-key";
		public static String StringXApiValue = "OFFICE_BING_API_KEY";


		public static String AUTHORIZATION_LABEL = "Authorization";
		public static String BEARER_LABEL = "Bearer ";
	}

	public static class OKHTTPVariables
	{
		public static int MaxTimeOutInMins = 7;
		public static TimeUnit ConnectionTimeOutUnit = TimeUnit.MINUTES;
	}

	public static class CommonValues
	{
		public static int MaxCitiesCanSelect = 3;
		public static String StringTimeline = "Sales Stage";
		public static String StringHome = "Home";
		public static String StringInvite = "Invite";
		public static String StringPlan = "Plan";
		public static String StringProfile = "Profile";
		public static String StringPurchased = "debit";

	}

	public static class IntentValues
	{
		public static String ViewTimeline = "viewTimeLine";
		public static String ViewLocations = "ViewLocations";
		public static String ViewDisputes = "ViewDisputes";
		public static String AddFollowup = "addDataTimeline";
		public static String ViewEditRequests = "viewEditRequestes";
	}

	public static class ExtractionConstatnts
	{
		public static String Success = "success";
		public static String True = "true";
		public static String Message = "message";
	}

	public static class LeadsConstants
	{
		public final static String KeyPending = "pending";
		public final static String KeyDiscarded = "decline";
		public final static String KeyAccepted = "accept";
		public final static String KeyConverted = "convert";

		public static String StringPending = "Pending";
		public static String StringDiscarded = "Cancelled";
		public static String StringAccepted = "Accepted";
		public static String StringConverted = "Converted";
	}

	    public static class BottomNavigationIds
		{
			public final static int MenuHome = R.id.men_home;
			public final static int MenuInvite = R.id.men_invite;
			public final static int MenuPlan = R.id.men_plan;
			public final static int MenuProfile = R.id.men_profile;
		}
	public static class WalletTabs
	{
		public static String StringTransactionTab = "Transaction History";
		public static String StringRechargeTab = "Payment History";
	}
}

