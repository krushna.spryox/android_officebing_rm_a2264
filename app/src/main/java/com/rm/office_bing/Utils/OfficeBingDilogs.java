package com.rm.office_bing.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rm.office_bing.Communicators.DialogCommunicator;
import com.rm.office_bing.Communicators.DialogType;
import com.rm.office_bing.Models.OfficeBingDialogModel;
import com.rm.office_bing.R;


public class OfficeBingDilogs implements DialogInterface.OnDismissListener, View.OnClickListener
{
	private final static OfficeBingDilogs objOfficeBingDialog = new OfficeBingDilogs();
	RecyclerView recyclerList = null;
	private OfficeBingDialogModel model;
	private Context context;
	private DialogType type;
	private Dialog dialog = null;
	private AppCompatEditText editDesc = null;
	private AppCompatButton btnYes = null, btnNo = null;
	private AppCompatImageView imgIcon;
	private AppCompatTextView txtHeader, txtMessage;
	private DialogCommunicator callBacker;
	private int Identifier;

	public static OfficeBingDilogs getInstance()
	{
		return objOfficeBingDialog;
	}

	public void initDialog(@NonNull Context context, @NonNull DialogType type, @NonNull DialogCommunicator callBacker, @NonNull OfficeBingDialogModel model, int Identifier)
	{
		if (dialog == null)
		{
			this.context = context;
			this.type = type;
			this.callBacker = callBacker;
			this.model = model;
			this.Identifier = Identifier;
			initVariables();
			initWidgets();
			initListeners();
			trasperent();
			setData();
		}
	}

	private void setData()
	{
		if (model.getImgPath() != null)
		{
			Glide.with(context).load(model.getImgPath()).into(imgIcon);
		}
		else
		{
            if (model.getIcon() != -1)
            {
                imgIcon.setImageDrawable(context.getResources().getDrawable(model.getIcon()));
            }
            else
            {
                imgIcon.setVisibility(View.GONE);
            }
		}
		txtHeader.setText(model.getTitle());
		txtMessage.setText(model.getMessage());

		if (recyclerList != null)
		{
			recyclerList.setLayoutManager(new LinearLayoutManager(context));
			recyclerList.setAdapter(model.getAdapter());
		}
	}

	private void initWidgets()
	{
		imgIcon = dialog.findViewById(R.id.img_dialog_icon);
		txtHeader = dialog.findViewById(R.id.txt_dialog_header);
		txtMessage = dialog.findViewById(R.id.txt_dialog_message);
	}

	private void initListeners()
	{
		dialog.setOnDismissListener(this);
		if (btnYes != null)
		{
			btnYes.setOnClickListener(this);
		}
		if (btnNo != null)
		{
			btnNo.setOnClickListener(this);
		}
	}

	private void initVariables()
	{
		switch (type)
		{
			case Select:
				dialog = new Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
				break;
			default:
				dialog = new Dialog(context);
				break;
		}
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		SetContentView();
		dialog.show();
	}

	private void trasperent()
	{
		if (dialog.getWindow() != null)
		{
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		}
	}

	private void SetContentView()
	{
		switch (type)
		{
			case Info:
				Informative();
				break;
			case Confirm:
				Confirmative();
				break;
			case Edit:
				Editive();
				break;
			case Select:
				Selective();
				break;
		}
	}

	private void Selective()
	{
		dialog.setContentView(R.layout.dialog_select);
		recyclerList = dialog.findViewById(R.id.recycler_list);
		btnYes = dialog.findViewById(R.id.btn_dialog_yes);
		btnNo = dialog.findViewById(R.id.btn_dialog_no);
		txtMessage = dialog.findViewById(R.id.txt_dialog_message);
	}

	private void Editive()
	{
		dialog.setContentView(R.layout.dialog_edit);
		btnYes = dialog.findViewById(R.id.btn_dialog_yes);
		btnNo = dialog.findViewById(R.id.btn_dialog_no);
		editDesc = dialog.findViewById(R.id.edit_desc);
	}

	private void Confirmative()
	{
		dialog.setContentView(R.layout.dialog_confirmation);
		btnYes = dialog.findViewById(R.id.btn_dialog_yes);
		btnNo = dialog.findViewById(R.id.btn_dialog_no);
	}

	private void Informative()
	{
		dialog.setContentView(R.layout.dialog_informative);
		btnNo = dialog.findViewById(R.id.btn_dialog_no);
	}


	@Override
	public void onDismiss(DialogInterface dialog)
	{
		DismissDialog();
	}

	public void DismissDialog()
	{
		if (dialog != null)
		{
			dialog.dismiss();
			dialog = null;
			btnNo = null;
			btnYes = null;
			editDesc = null;
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btn_dialog_yes:
				if (editDesc != null)
				{
					EditiveReactingToCallBack();
				}
				else
				{
					ConfirmativeReactingToCallBack();
				}
				break;
			case R.id.btn_dialog_no:
				callBacker.whatDialogSaid(null, false, Identifier);
				DismissDialog();
				break;
		}
	}

	private void ConfirmativeReactingToCallBack()
	{
		callBacker.whatDialogSaid(null, true, Identifier);
		DismissDialog();
	}

	private void EditiveReactingToCallBack()
	{
		if (editDesc != null)
		{
            if (editDesc.getText() != null)
            {
                callBacker.whatDialogSaid(editDesc.getText().toString(), true, Identifier);
            }
            else
            {
                callBacker.whatDialogSaid(null, true, Identifier);
            }
			DismissDialog();
		}
		else
		{
			Toast.makeText(context, ToastMessages.FieldCantBlank, Toast.LENGTH_SHORT).show();
			editDesc.setError(ToastMessages.FieldCantBlank);
		}
	}
}
