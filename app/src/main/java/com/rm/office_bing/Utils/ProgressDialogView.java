package com.rm.office_bing.Utils;


import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogView
{

	private static ProgressDialog progressBar;
	private Context context = null;

	public ProgressDialogView(Context context)
	{
		this.context = context;
	}

	public void showProgress()
	{
		if (progressBar == null)
		{
			if (context != null)
			{
				progressBar = new ProgressDialog(context);
			}

			if (progressBar != null)
			{
				progressBar.setCancelable(false);//you can cancel it by pressing back button
				progressBar.setMessage("Loading...");
				progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressBar.show();
			}
		}
	}

	public void hideProgress()
	{
		if (progressBar != null)
		{
			if (progressBar.isShowing())
			{
				progressBar.dismiss();
			}
		}
		context = null;
		progressBar = null;
	}

}
