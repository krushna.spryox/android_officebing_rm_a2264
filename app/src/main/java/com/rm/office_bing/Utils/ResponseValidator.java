package com.rm.office_bing.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseValidator
{
	static String TAG = "ResponseValidator";

	public static boolean isValidResponse(String Response, Context context, boolean wantToast)
	{
		JSONObject jsonObject = null;
		if (Response != null)
		{
			try
			{
				jsonObject = new JSONObject(Response);
				String status = jsonObject.getString(ConstantVariables.ExtractionConstatnts.Success);
				String message = jsonObject.getString(ConstantVariables.ExtractionConstatnts.Message);
				Log.d(TAG, "isValidResponse: message: " + message);
				Log.d(TAG, "isValidResponse: status: " + status);
				if (status.contains(ConstantVariables.ExtractionConstatnts.True))
				{
					if (wantToast)
					{
						Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				}
				return status.contains(ConstantVariables.ExtractionConstatnts.True);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				Log.d(TAG, "isValidResponse: JSON ERROR " + Response);
			}
		}
		return false;
	}

}
