package com.rm.office_bing.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.Toast;

import com.rm.office_bing.Activities.LoginActivity;

public class SessionManager
{
	public static final String FCM_TOKEN = "fcm_token";
	public static final String API_TOKEN = "token";
	public static final String KEY_ID = "user_id";
	public static final String KEY_FIRST_NAME = "fname";
	public static final String KEY_LAST_NAME = "lname";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_SPACE_ID = "Space_lead_id";
	public static final String KEY_LEAD_ID = "Lead_id";
	public static final String KEY_COMPANY_NAME = "company_name";
	public static final String KEY_PLAN_ID = "plan_id";
	public static final String KEY_GST_NO = "gst_no";
	public static final String KEY_MOB_NO = "mobile";
	public static final String KEY_PLAN_NAME = "plan_name";
	public static final String KEY_WALLET_AMT = "amt";
	public static final String KEY_IS_LOGGEDIN = "is_login";


	private static String TAG = SessionManager.class.getSimpleName();
	// Shared Preferences
	SharedPreferences pref;
	// Editor for Shared preferences
	Editor editor;
	Context _context;
	// Shared pref mode
	int PRIVATE_MODE = 0;

	//constructor
	public SessionManager(Context context)
	{
		this._context = context;
		pref = _context.getSharedPreferences(context.getPackageName(), PRIVATE_MODE);
		editor = pref.edit();
	}

	public static String getData(Context ctx, String key)
	{

		SharedPreferences preferences = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
		return preferences.getString(key, "");
	}


	public String getFcmToken()
	{
		String fcmToken = "";
		fcmToken = pref.getString(FCM_TOKEN, "");
		return fcmToken;
	}

	public void setFcmToken(String fcmToken)
	{
		// Storing name in pref
		editor.putString(FCM_TOKEN, fcmToken);
		// commit changes
		editor.commit();
	}

	public String getAPIToken()
	{
		String fcmToken = "";
		// fcmToken
		fcmToken = pref.getString(API_TOKEN, "");
		// return fcmToken
		return fcmToken;
	}

	public void setAPIToken(String apiToken)
	{
		// Storing name in pref
		editor.putString(API_TOKEN, apiToken);
		// commit changes
		editor.commit();
	}

	public String getFirstName()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_FIRST_NAME, "");
		// return user
		return name;
	}

	public void setFirstName(String firstName)
	{
		// Storing name in pref
		editor.putString(KEY_FIRST_NAME, firstName);
		// commit changes
		editor.commit();
	}

	public String getUserID()
	{
		String employee = "";
		// user name
		employee = pref.getString(KEY_ID, "");
		// return user
		return employee;
	}

	public void setUserID(String employee_id)
	{
		// Storing name in pref
		editor.putString(KEY_ID, employee_id);
		// commit changes
		editor.commit();
	}

	public String getLastName()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_LAST_NAME, "");
		// return user
		return name;
	}

	public void setLastName(String lastName)
	{
		// Storing name in pref
		editor.putString(KEY_LAST_NAME, lastName);
		// commit changes
		editor.commit();
	}

	public String getEmail()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_EMAIL, "");
		// return user
		return name;
	}

	public void setEmail(String fullName)
	{
		// Storing name in pref
		editor.putString(KEY_EMAIL, fullName);
		// commit changes
		editor.commit();
	}

	public String getInitial()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_EMAIL, "");
		// return user
		return name;
	}

	public void setInitial(String fullName)
	{
		// Storing name in pref
		editor.putString(KEY_EMAIL, fullName);
		// commit changes
		editor.commit();
	}

	public String getCompanyName()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_COMPANY_NAME, "");
		// return user
		return name;
	}

	public void setCompanyName(String companyName)
	{
		// Storing name in pref
		editor.putString(KEY_COMPANY_NAME, companyName);
		// commit changes
		editor.commit();
	}

	public String getPlanId()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_PLAN_ID, "");
		// return user
		return name;
	}

	public void setPlanId(String planId)
	{
		editor.putString(KEY_PLAN_ID, planId);
		editor.commit();
	}

	public String getSpaceLeadId()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_SPACE_ID, "");
		// return user
		return name;
	}

	public void setSpaceLeadId(String spaceId)
	{
		editor.putString(KEY_SPACE_ID, spaceId);
		editor.commit();
	}

	public String getLeadId()
	{
		String name = "";
		// user name
		name = pref.getString(KEY_LEAD_ID, "");
		// return user
		return name;
	}

	public void setLeadId(String spaceId)
	{
		editor.putString(KEY_LEAD_ID, spaceId);
		editor.commit();
	}

	public String getGSTNo()
	{
		return pref.getString(KEY_GST_NO, "");
	}

	public void setGSTNo(String gstNo)
	{
		// Storing name in pref
		editor.putString(KEY_GST_NO, gstNo);
		// commit changes
		editor.commit();
	}

	public String getMobileNo()
	{
		return pref.getString(KEY_MOB_NO, "");
	}

	public void setMobileNo(String mobileNo)
	{
		// Storing name in pref
		editor.putString(KEY_MOB_NO, mobileNo);
		// commit changes
		editor.commit();
	}

	public String getPlanName()
	{
		return pref.getString(KEY_PLAN_NAME, "");
	}

	public void setPlanName(String planName)
	{
		editor.putString(KEY_PLAN_NAME, planName);
		editor.commit();
	}

	public String getWalletAmt()
	{
		return pref.getString(KEY_WALLET_AMT, "");
	}

	public void setWalletAmt(String amt)
	{
		editor.putString(KEY_WALLET_AMT, amt);
		editor.commit();
	}

	public void setLogin(boolean isLoggedIn)
	{

		editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

		// commit changes
		editor.commit();

		Log.d(TAG, "User login session modified!");
	}

	public boolean isLoggedIn()
	{
		return pref.getBoolean(KEY_IS_LOGGEDIN, false);
	}

	/**
	 * Clear session details
	 */
	public void logoutUser()
	{
		// Clearing all data from Shared Preferences
		Toast.makeText(_context, ToastMessages.LoggingUserOut, Toast.LENGTH_SHORT).show();
		String GetFCM = getFcmToken();
		editor.clear();
		editor.commit();
		setFcmToken(GetFCM);
		Intent i = new Intent(_context, LoginActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		_context.startActivity(i);
	}
}
