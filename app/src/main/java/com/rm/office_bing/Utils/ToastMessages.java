package com.rm.office_bing.Utils;

public class ToastMessages
{
	public final static String TryAgain = "AddClientData Can't Load At This Moment Try Again.!";
	public final static String NoInternet = "Check Internet Connection";
	public final static String ModifiedSetting = "Setting Modified.!";
	public final static String Syncing = "Syncing User Settings";
	public final static String Syncingdesc = "Seems  you changed settings. Syncing your Settings.";
	public static String SelectImageFirst = "Please Select The Image First.!";
	public static String AddedClient = "Client Added Successfully.!";
	public static String ClientcantDeleted = "Client can't Deleted. Try Again.!";
	public static String ClientDeleted = "Client Deleted SuccessFully!";
	public static String MailFormatError = "Email Not in format.!";
	public static String MobileNumberFormatError = "Mobile Number should be more than 6 digits.!";
	public static String FieldCantBlank = "Field Can't be blank.!";
	public static String DeleteClient = "Deleting Client";
	public static String EditingClient = "Fetching Client Details.!";
	public static String MakingCall = "Making Call";
	public static String IdCantNull = "InSufficient InvoiceListData to Edit Client.!";
	public static String LoggingUserOut = "Logging User Out.!";
	public static String closingDialog = "Closing Dialog.!";
	public static String AddingNewAddress = "Adding New Address To Client.!";
	public static String PasswordNotSame = "Password & Confirm Password Doesn't Match.!";
	public static String PasswordError = "Minimum Password Should Be 6 Characters Long";
	public static String SelectFromList = "Select Item From List First";

	public static String AddAreaToContinue = "Please Add Area To continue.!";
	public static String LogoMendetory = "Logo Mendatory.!";
	public static String alreadyLoggedIn = "Already Logged In.!";
	public static String LoggedIn = "Loggedbe In.!";
	public static String StringLinkCopied = "Link Copied";
	public static String StringAddFollowup = "Add Followup To Timeline";
	public static String StringNoDispute = "No Dispute Added";
	public static String StringNoCityPrefs = "No City Preferences";
	public static String StringAcceptLead = "Sure Accepting The Lead?";
	public static String StringCancelLead = "Sure Rejecting The Lead?";
	public static String StringCancelMessage = "By clicking on Yes.Lead will be rejected";
	public static String StringAcceptLeadMessage = "By clicking on Yes. Lead will be accepted and appropriate money would be deducted from your wallet";
	public static String AdditionaCommnt="Addtional Comment (Optional)";
	public static String AddRevenueError="Add Revenue To Continue";
	public static String RevennueTexxt="Client Revenue(Required)";
	public static String RevenueError="Revenue can be numbers only.!";

	public static String ErrorOccurred = "Error occurred.Please try again.!";
	public static String NoRejectionList = "No rejection list found";
	public static String InternalError = "Internal error.Contact admin for more information";
	public static String DataLoadError = "No space modification request found";
	public static String SpaceNotListed = "This space is not listed yet. Please contact admin.!";
	public static String RequestAlreadyGot = "We have already received request to change data.!";
}
