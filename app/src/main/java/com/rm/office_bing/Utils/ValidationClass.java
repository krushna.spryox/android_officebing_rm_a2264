package com.rm.office_bing.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatEditText;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationClass
{
	private static final String TAG = "Validation";

	public static boolean isValidEmail(AppCompatEditText Field)
	{
		if (isEmpty(Field))
		{
			return false;
		}
		String Email = Field.getText().toString().trim();
		Log.d(TAG, "isValidEmail:Email " + Email);
		String pattern = "^[a-zA-Z0-9_+&*-]+(?:\\." +
				"[a-zA-Z0-9_+&*-]+)*@" +
				"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
				"A-Z]{2,7}$";
		Pattern pat = Pattern.compile(pattern);
		boolean Emails = pat.matcher(Email).matches();
		Log.d(TAG, "isValidEmail:Emails " + Emails);
		if (Emails)
		{
			Log.d(TAG, "isValidEmail:EMail True ");
			return true;
		}
		else
		{
			Field.setError(ToastMessages.MailFormatError);
			return false;
		}
	}

	public static boolean isEmpty(AppCompatEditText Field)
	{
		if (Field != null)
		{
			if (!(Field.getText() != null && Field.getText().toString().isEmpty()))
			{
				return false;
			}
			else
			{
				Log.d(TAG, "NullField:EditClientData Empty ");
				Field.setError(ToastMessages.FieldCantBlank);
			}
		}
		return true;
	}

	public static boolean isListEmpty(@NotNull List<AppCompatEditText> Fields)
	{
		boolean listEmpty = true;
		for (AppCompatEditText Field : Fields)
		{
			if (Field != null)
			{
				if (!(Field.getText() != null && Field.getText().toString().isEmpty()))
				{
					listEmpty = false;
				}
				else
				{
					Log.d(TAG, "NullField:EditClientData Empty ");
					Field.setError(ToastMessages.FieldCantBlank);
					return true;
				}
			}
		}
		return listEmpty;
	}

	public static boolean isValidPassword(AppCompatEditText password)
	{
		if (isEmpty(password))
		{
			return false;
		}
		String regExpn = "^[a-z0-9_$@.!%*?&]{6,24}$";
		Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(Objects.requireNonNull(password.getText()).toString());
		if (matcher.matches())
		{
			return true;
		}
		else
		{
			password.setError(ToastMessages.PasswordError);
		}
		return false;
	}

	public static int dpToPx(Context c, int dp)
	{
		Resources r = c.getResources();
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
	}
}
