package com.rm.office_bing.fragments.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Activities.MyLeadsActivity;
import com.rm.office_bing.Adapters.LeadsAdapter.LeadsAdapter;
import com.rm.office_bing.Communicators.DialogCommunicator;
import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.HomeModels.TextWithImageModel;
import com.rm.office_bing.Models.LeadsModels.ListLeads.ListLeadsItem;
import com.rm.office_bing.Models.LeadsModels.ListLeads.ListLeadsResponse;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.rm.office_bing.Utils.ConstantVariables.ApiValues.StringIdKey;
import static com.rm.office_bing.Utils.ConstantVariables.ApiValues.StringPageKey;
import static com.rm.office_bing.Utils.ConstantVariables.ApiValues.StringTypeKey;
import static com.rm.office_bing.Utils.ConstantVariables.LeadsConstants.KeyPending;

public class HomeFragment extends Fragment implements View.OnClickListener, DialogCommunicator, ResponseCarrier
{
	private final int pendingLeadsIdentifier = 1003;
	String TAG = "HomeFragment";
	AppCompatTextView txtNoLeads;
	NetworkCaller networkCaller;
	SessionManager manager;
	ProgressDialogView progressDialogView;
	AppCompatButton btnRefillWallet;
	ReplaceFragment replaceFragment;
	AppCompatTextView txtWalletBalance;
	LeadsAdapter adapter;
	ProgressBar progressLoadingData;
	private View view;
	private Context context;
	private LinearLayoutCompat linearMySpace, linearMyLeads, linearMyWallet;
	private RecyclerView recyclerNavigation, recyclerLeads;
	private List<TextWithImageModel> dataToFillNavigation;
	private List<ListLeadsItem> dataToFillLeads;
	private int PageCount = 1;
	private LinearLayoutManager LeadsManager;

	public HomeFragment(ReplaceFragment replaceFragment)
	{
		this.replaceFragment = replaceFragment;
	}

	public HomeFragment()
	{

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_home, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();

		init();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		//init();
		getHomeLeads();
	}

	private void init()
	{
		initWidgets();
		initVariables();
		initListeners();
		setData();
		//setNonEmptyDataToNavigationList();

	}

	@Override
	public void onResume()
	{
		super.onResume();

	}

	private void getHomeLeads()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.MyLeads, getRequestBodyForHomeDetails(), pendingLeadsIdentifier, true);
	}

	private void setData()
	{
		btnRefillWallet.setBackground(context.getResources().getDrawable(R.drawable.red_back));
		btnRefillWallet.setText(R.string.refill);
		txtNoLeads.setVisibility(View.GONE);
		txtWalletBalance.setText("Rs." + manager.getWalletAmt());
	}

	private void initListeners()
	{
		linearMySpace.setOnClickListener(this);
		linearMyLeads.setOnClickListener(this);
		linearMyWallet.setOnClickListener(this);
		btnRefillWallet.setOnClickListener(this);
		recyclerLeads.setOnScrollListener(new RecyclerScroll());
	}

	private void initVariables()
	{
		PageCount = 1;
		dataToFillNavigation = new ArrayList<>();
		dataToFillLeads = new ArrayList<>();
		progressDialogView = new ProgressDialogView(context);
		networkCaller = new NetworkCaller(context, this);
		manager = new SessionManager(context);
		LeadsManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);

	}

	private void initWidgets()
	{
		recyclerNavigation = view.findViewById(R.id.recycler_navigations);
		progressLoadingData = view.findViewById(R.id.progress_loading_data);
		txtNoLeads = view.findViewById(R.id.txt_no_leads);
		recyclerLeads = view.findViewById(R.id.recycler_leads);
		linearMySpace = view.findViewById(R.id.linear_my_space);
		linearMyLeads = view.findViewById(R.id.linear_my_leads);
		linearMyWallet = view.findViewById(R.id.linear_my_wallet);
		btnRefillWallet = view.findViewById(R.id.btn_fill_wallet);
		txtWalletBalance = view.findViewById(R.id.txt_wallet_amt);
	}

	private void setNonEmptyDataToLeadList()
	{
		if (PageCount > 1 && adapter != null)
		{
			adapter.setData(dataToFillLeads);
			DoneRefreshing();
			return;
		}
		recyclerLeads.setLayoutManager(LeadsManager);
		adapter = new LeadsAdapter(context, dataToFillLeads);
		recyclerLeads.setAdapter(adapter);
	}

	private void DoneRefreshing()
	{
		progressLoadingData.setVisibility(View.GONE);
	}


	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.linear_my_space:
				//startActivity(new Intent(context, MySpacesActivity.class));
				break;
			case R.id.linear_my_leads:
				startActivity(new Intent(context, MyLeadsActivity.class));
				break;
			case R.id.linear_my_wallet:
				//startActivity(new Intent(context, WalletActivity.class));
				break;
			case R.id.btn_fill_wallet:
				Log.d(TAG, "onClick:R.id.btn_fill_wallet ");
				replaceFragment.replace(new PlanFragment(replaceFragment));
				break;
		}
	}

	@Override
	public void whatDialogSaid(Serializable serializable, boolean WhichClicked, int Identifier)
	{
		Log.d(TAG, "whatDialogSaid: WhichClicked " + WhichClicked);
		String s = (String) serializable;
		Log.d(TAG, "whatDialogSaid: " + s);
	}

	private JsonObject getRequestBodyForHomeDetails()
	{
		JsonObject object = new JsonObject();
		object.addProperty(StringIdKey, manager.getUserID());
		object.addProperty(StringTypeKey, KeyPending);
		object.addProperty(StringPageKey, PageCount);
		return object;
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case pendingLeadsIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractHomeData(Response);
				}
				else
				{
					NoLeadsData();
				}
				break;
		}
	}

	private void NoLeadsData()
	{
		if (PageCount == 1)
		{
			recyclerLeads.setVisibility(View.GONE);
			txtNoLeads.setVisibility(View.VISIBLE);
		}
		else
		{
			DoneRefreshing();
		}
	}

	private void ExtractHomeData(String response)
	{
		try
		{
			ListLeadsResponse listLeadsResponse = new Gson().fromJson(response, ListLeadsResponse.class);
			if (listLeadsResponse.getData() == null)
			{
				Log.d(TAG, "ExtractHomeData: listLeadsResponse.getData()==null");
				return;
			}
			dataToFillLeads = listLeadsResponse.getData().getLeads();

			if (dataToFillLeads.size() > 0)
			{
				setNonEmptyDataToLeadList();
				PageCount++;
			}
			else
			{
				NoLeadsData();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e(TAG, "HomeDetailsException:Error Extracting Leads Data" + e);
			NoLeadsData();
		}

	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		Log.e(TAG, "HomeDetailsResponseError" + Response);
		switch (Identifier)
		{
			case pendingLeadsIdentifier:
				NoLeadsData();
				break;
		}
	}

	void Refreshing()
	{
		progressLoadingData.setVisibility(View.VISIBLE);
	}

	public void reload()
	{
		init();
	}

	class RecyclerScroll extends RecyclerView.OnScrollListener
	{
		@Override
		public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState)
		{
			if (LeadsManager.findLastVisibleItemPosition() == adapter.modelList.size() - 1)
			{
				Toast.makeText(context, "Refreshing", Toast.LENGTH_SHORT).show();
				Refreshing();
				getHomeLeads();
			}
			super.onScrollStateChanged(recyclerView, newState);
		}
	}
}
