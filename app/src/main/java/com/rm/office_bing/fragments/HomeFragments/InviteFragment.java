package com.rm.office_bing.fragments.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFragment extends Fragment implements View.OnClickListener
{

	View view;
	Context context;
	AppCompatTextView txtShareLink;
	ReplaceFragment replaceFragment;

	public InviteFragment()
	{
		// Required empty public constructor
	}

	public InviteFragment(ReplaceFragment replaceFragment)
	{
		this.replaceFragment = replaceFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_invite, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		initWidgets();
		initVariables();
		initListeners();
	}

	private void initVariables()
	{
	}

	private void initListeners()
	{
		txtShareLink.setOnClickListener(this);
	}

	private void initWidgets()
	{
		txtShareLink = view.findViewById(R.id.txt_share_link);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.txt_share_link:
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT, "www.google.com");
				sendIntent.setType("text/plain");
				Intent shareIntent = Intent.createChooser(sendIntent, "Share Link");
				startActivity(shareIntent);
				break;
		}
	}
}
