package com.rm.office_bing.fragments.HomeFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.PlanAdapters.PlanAdapter;
import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.Plans.ListPlans.ListPlansDataItem;
import com.rm.office_bing.Models.Plans.ListPlans.ListPlansResponse;
import com.rm.office_bing.Models.Plans.PlansModel;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlanFragment extends Fragment implements ResponseCarrier
{

	private final int PlansIdentifier = 229;
	RecyclerView plan_recycler;
	View rootView;
	PlanAdapter planAdapter;
	ArrayList<PlansModel> plansModels = new ArrayList<>();
	Context context;
	NetworkCaller networkCaller;
	SessionManager sessionManager;
	TextView txtNoPlans;
	ProgressDialogView progressDialogView;
	List<ListPlansDataItem> listPlans;
	ReplaceFragment replaceFragment;

	public PlanFragment()
	{
		// Required empty public constructor
	}

	public PlanFragment(ReplaceFragment replaceFragment)
	{
		this.replaceFragment = replaceFragment;
	}


	@Override
	public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_plans, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		rootView = view;
		context = rootView.getContext();
		initViews();
		initVariables();
		initListeners();

	}

	@Override
	public void onResume()
	{
		super.onResume();
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.Plans, getRequestBodyForPlans(), PlansIdentifier, true);
	}

	private JsonObject getRequestBodyForPlans()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		return jsonObject;
	}

	private void initViews()
	{
		txtNoPlans = rootView.findViewById(R.id.txt_no_plans);
		plan_recycler = rootView.findViewById(R.id.plan_recycler);
	}

	private void initVariables()
	{
		networkCaller = new NetworkCaller(context, this);
		sessionManager = new SessionManager(context);
		progressDialogView = new ProgressDialogView(context);
		listPlans = new ArrayList<>();
	}

	private void initListeners()
	{
	}


	private void NonEmptyPlanList()
	{
		plan_recycler.setLayoutManager(new GridLayoutManager(context, 2));
		planAdapter = new PlanAdapter(context, listPlans, sessionManager);
		plan_recycler.setAdapter(planAdapter);

	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case PlansIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, true))
				{
					ListPlansResponse listPlansResponse = new Gson().fromJson(Response, ListPlansResponse.class);
					listPlans = listPlansResponse.getData();
					if (listPlans != null && listPlans.size() > 0)
					{
						NonEmptyPlanList();
					}
					else
					{
						NoPlanData();
					}
				}
				else
				{
					NoPlanData();
				}
				break;
		}
	}

	private void NoPlanData()
	{
		plan_recycler.setVisibility(View.GONE);
		txtNoPlans.setVisibility(View.VISIBLE);
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case PlansIdentifier:
				NoPlanData();
				break;
		}
	}
}
