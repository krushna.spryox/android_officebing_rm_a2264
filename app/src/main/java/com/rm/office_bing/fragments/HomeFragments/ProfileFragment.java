package com.rm.office_bing.fragments.HomeFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment
{
	SessionManager sessionManager;
	ReplaceFragment replaceFragment;
	ConstraintLayout constraintGoToWallet;
	private AppCompatTextView txtName, txtMobile, txtEmail, txtCompanyName, txtGSTNo, txtInitial, txtWalletAmount;
	private View view;
	private Context context;

	public ProfileFragment()
	{
		// Required empty public constructor
	}

	public ProfileFragment(ReplaceFragment replaceFragment)
	{
		this.replaceFragment = replaceFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_profile, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		initViews();
		initVariables();
		initListeners();
		setData();
	}

	private void setData()
	{
		txtInitial.setText(sessionManager.getInitial());
		txtGSTNo.setText(sessionManager.getGSTNo());
		txtMobile.setText(sessionManager.getMobileNo());
		txtEmail.setText(sessionManager.getEmail());
		txtName.setText(sessionManager.getFirstName());
		txtCompanyName.setText(sessionManager.getCompanyName());
		txtWalletAmount.setText("Rs." + sessionManager.getWalletAmt());
	}

	private void initViews()
	{
		txtCompanyName = view.findViewById(R.id.txt_company_name);
		constraintGoToWallet = view.findViewById(R.id.linear_to_wallet);
		txtName = view.findViewById(R.id.txt_user_name);
		txtEmail = view.findViewById(R.id.txt_email);
		txtMobile = view.findViewById(R.id.txt_mobile_num);
		txtGSTNo = view.findViewById(R.id.txt_gst_no);
		txtInitial = view.findViewById(R.id.txt_initial);
		txtWalletAmount = view.findViewById(R.id.txt_wallet_amt);
	}

	private void initVariables()
	{
		sessionManager = new SessionManager(context);
	}

	private void initListeners()
	{
		//constraintGoToWallet.setOnClickListener(v -> startActivity(new Intent(context, WalletActivity.class)));
	}
}
