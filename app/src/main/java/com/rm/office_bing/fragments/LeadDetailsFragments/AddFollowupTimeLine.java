package com.rm.office_bing.fragments.LeadDetailsFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.AcceptedLeadsAdapters.ReasonsStatusAdapter;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsDataItem;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsResponse;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.Utils.ToastMessages;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddFollowupTimeLine extends Fragment implements ResponseCarrier, View.OnClickListener, NetworkCallbacker
{
	private final int listFollowupIdentifier = 192;
	String TAG = "AddFollowupTimeLine";
	NetworkCaller networkCaller;
	ProgressDialogView progressDialogView;
	SessionManager sessionManager;
	List<ListReasonsDataItem> listFollowups;
	AppCompatTextView txtNoFollowup;
	FloatingActionButton fabViewTimeline;
	private ReplaceFragment fragment;
	private Button btnContinue;
	private View view;
	private Context context;
	private RecyclerView recyclerFollowups;

	public AddFollowupTimeLine(ReplaceFragment replaceFragment)
	{
		fragment = replaceFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// addFollowupTimeLineBinding=FragmentAddFollowupTimeLineBinding.inflate(getLayoutInflater());
		return inflater.inflate(R.layout.fragment_add_followup_time_line, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		//Preprocessing();
		initWidgets();
		initVariables();
		initListeners();
		setData();

	}

	@Override
	public void onStart()
	{
		super.onStart();
		Preprocessing();
		getFollowups();
	}

	private void getFollowups()
	{
		if (!Objects.equals(sessionManager.getSpaceLeadId(), "-1"))
		{
			progressDialogView.showProgress();
			networkCaller.initPostCallWithBody(ApiNodes.ListReasons, getBodyForlistFollowup(), listFollowupIdentifier, true);
		}
		else
		{
			NoFollowup();
		}
	}

	void NoFollowup()
	{
		txtNoFollowup.setVisibility(View.VISIBLE);
		recyclerFollowups.setVisibility(View.GONE);
	}

	void HasFollowup()
	{
		txtNoFollowup.setVisibility(View.GONE);
		recyclerFollowups.setVisibility(View.VISIBLE);
	}

	@NotNull
	private JsonObject getBodyForlistFollowup()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, sessionManager.getSpaceLeadId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringTypeKey, ConstantVariables.ApiValues.StringFollowupType);
		Log.d(TAG, "getBodyForlistFollowup: jsonObject: " + jsonObject);
		return jsonObject;
	}

	private void Preprocessing()
	{
		NetworkConnectionBrods.getInstance().setData(this, context);
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
		new SetHeader(view.findViewById(R.id.txt_header), ToastMessages.StringAddFollowup, true);
		new BackButton(Objects.requireNonNull(getActivity()), view.findViewById(R.id.img_back), true);
		new OpenMenu(context, view.findViewById(R.id.img_menu), R.menu.home_option_menu, false);
	}

	private void setData()
	{
		btnContinue.setText("Add Followup");

	}

	private void initWidgets()
	{
		btnContinue = view.findViewById(R.id.btn_continue);
		recyclerFollowups = view.findViewById(R.id.recycler_followups);
		txtNoFollowup = view.findViewById(R.id.txt_no_followups);
		fabViewTimeline = view.findViewById(R.id.fab_view_timeline);
	}

	private void initVariables()
	{
		networkCaller = new NetworkCaller(context, this);
		sessionManager = new SessionManager(context);
		progressDialogView = new ProgressDialogView(context);
		listFollowups = new ArrayList<>();
	}

	private void initListeners()
	{
		fabViewTimeline.setOnClickListener(this);
	}

	void SetNonEmptyDataToFollowupList()
	{
		recyclerFollowups.setLayoutManager(new LinearLayoutManager(context));
		recyclerFollowups.setAdapter(new ReasonsStatusAdapter(listFollowups, context, ConstantVariables.ApiValues.StringFollowupType));
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listFollowupIdentifier:
                if (ResponseValidator.isValidResponse(Response, context, false))
                {
                    ExtractDataOfFollowupList(Response);
                }
				break;
		}
	}

	private void ExtractDataOfFollowupList(String response)
	{
		try
		{
			ListReasonsResponse listFollowupResponse = new Gson().fromJson(response, ListReasonsResponse.class);
			if (listFollowupResponse.getData() != null && listFollowupResponse.getData().size() > 0)
			{
				listFollowups = listFollowupResponse.getData();
				HasFollowup();
				SetNonEmptyDataToFollowupList();
			}
			else
			{
				NoFollowup();
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataOfFollowupList: Error Extracting list Followup" + e.getLocalizedMessage());
			NoFollowup();
		}
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listFollowupIdentifier:
				NoFollowup();
				break;
		}

	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.fab_view_timeline:
				fragment.replace(new ViewTimelineFragment(fragment));
				break;
		}
	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
		if (status)
			getFollowups();
	}
}
