package com.rm.office_bing.fragments.LeadDetailsFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.AcceptedLeadsAdapters.ReasonsStatusAdapter;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsDataItem;
import com.rm.office_bing.Models.FollowupModels.ReasonsList.ListReasonsResponse;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectDisputesFragment extends Fragment implements ResponseCarrier, NetworkCallbacker
{
	private final int listDisputeIdentifier = 182;
	String TAG = "SelectDisputesFragment";
	AppCompatButton btnCotinue;
	RecyclerView recyclerCities;
	NetworkCaller networkCaller;
	ProgressDialogView progressDialogView;
	SessionManager sessionManager;
	AppCompatTextView txtNoDisputes;
	private View view;
	private Context context;
	private List<ListReasonsDataItem> listDisputes;

	public SelectDisputesFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_select_disputes, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		initWidgets();
		initVariables();
		initListeners();
		setData();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Preprocessing();
		getDisputes();
	}

	private void getDisputes()
	{
		if (!Objects.equals(sessionManager.getSpaceLeadId(), "-1"))
		{
			progressDialogView.showProgress();
			networkCaller.initPostCallWithBody(ApiNodes.ListReasons, getBodyForlistDisputes(), listDisputeIdentifier, true);
		}
		else
		{
			NoDisputes();
		}
	}

	private void NoDisputes()
	{
		txtNoDisputes.setVisibility(View.VISIBLE);
		recyclerCities.setVisibility(View.GONE);
	}

	private void Preprocessing()
	{
		NetworkConnectionBrods.getInstance().setData(this, context);
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
		new BackButton(Objects.requireNonNull(getActivity()), view.findViewById(R.id.img_back), true);
		new OpenMenu(context, view.findViewById(R.id.img_menu), R.menu.home_option_menu, false);
		new SetHeader(view.findViewById(R.id.txt_header), context.getString(R.string.mark_dispute), true);
	}

	private void setData()
	{
		setNonEmptyDataToDisputeList();

	}

	private void initWidgets()
	{
		btnCotinue = view.findViewById(R.id.btn_continue);
		txtNoDisputes = view.findViewById(R.id.txt_no_disputes);
		recyclerCities = view.findViewById(R.id.recycler_cities);
	}

	private void initVariables()
	{
		networkCaller = new NetworkCaller(context, this);
		sessionManager = new SessionManager(context);
		progressDialogView = new ProgressDialogView(context);
		listDisputes = new ArrayList<>();
	}

	@NotNull
	private JsonObject getBodyForlistDisputes()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, sessionManager.getSpaceLeadId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringTypeKey, ConstantVariables.ApiValues.StringDisputeType);
		Log.d(TAG, "getBodyForlistFollowup: jsonObject: " + jsonObject);
		return jsonObject;
	}

	private void initListeners()
	{
	}

	void setNonEmptyDataToDisputeList()
	{
		recyclerCities.setLayoutManager(new LinearLayoutManager(context));
		recyclerCities.setAdapter(new ReasonsStatusAdapter(listDisputes, context, ConstantVariables.ApiValues.StringDisputeType));
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listDisputeIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractDataForDisputeList(Response);
				}
				else
				{
					NoDisputes();
				}
				break;
		}
	}

	private void ExtractDataForDisputeList(String response)
	{
		try
		{
			ListReasonsResponse listFollowupResponse = new Gson().fromJson(response, ListReasonsResponse.class);
			if (listFollowupResponse.getData() != null && listFollowupResponse.getData().size() > 0)
			{
				listDisputes = listFollowupResponse.getData();
				HasDisputes();
				setNonEmptyDataToDisputeList();
			}
			else
			{
				NoDisputes();
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataOfFollowupList: Error Extracting list Followup" + e.getLocalizedMessage());
			NoDisputes();
		}
	}

	private void HasDisputes()
	{
		txtNoDisputes.setVisibility(View.GONE);
		recyclerCities.setVisibility(View.VISIBLE);
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listDisputeIdentifier:
				NoDisputes();
				break;
		}
	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
        if (status)
        {
            getDisputes();
        }
	}
}
