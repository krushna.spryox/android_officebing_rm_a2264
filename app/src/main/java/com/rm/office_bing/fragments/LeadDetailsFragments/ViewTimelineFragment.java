package com.rm.office_bing.fragments.LeadDetailsFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.AcceptedLeadsAdapters.ViewTimelineAdapter;
import com.rm.office_bing.Brodcasts.NetworkConnectionBrods;
import com.rm.office_bing.Communicators.NetworkCallbacker;
import com.rm.office_bing.Communicators.ReplaceFragment;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.TimelineModels.ListTimeline.ListTimelineDataItem;
import com.rm.office_bing.Models.TimelineModels.ListTimeline.ListTimelineResponse;
import com.rm.office_bing.ParentActivity.BackButton;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.ParentActivity.OpenMenu;
import com.rm.office_bing.ParentActivity.SetHeader;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewTimelineFragment extends Fragment implements ResponseCarrier, NetworkCallbacker
{
	final int listTimelineIdentifier = 108;
	String TAG = "ViewTimelineFragment";
	SessionManager sessionManager;
	NetworkCaller networkCaller;
	ProgressDialogView progressDialogView;
	private RecyclerView recyclerTimeline;
	private View view;
	private Context context;
	private FloatingActionButton fabAddFollowUp;
	private ReplaceFragment fragment;
	private List<ListTimelineDataItem> listTimeline;
	private View noData;

	public ViewTimelineFragment(ReplaceFragment replaceFragment)
	{
		fragment = replaceFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_view_timeline, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		context = view.getContext();
		this.view = view;
		initWidgets();
		initVariables();
		initListeners();
		//setNonEmptyDataToTimelineRecycler();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Preprocessing();
		getTimelineList();
	}

	private void getTimelineList()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.ListTimeline, getBodyForlistTimeline(), listTimelineIdentifier, true);
	}

	private void Preprocessing()
	{
		NetworkConnectionBrods.getInstance().setData(this, context);
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
		new BackButton(Objects.requireNonNull(getActivity()), view.findViewById(R.id.img_back), true);
		new OpenMenu(context, view.findViewById(R.id.img_menu), R.menu.home_option_menu, false);
		new SetHeader(view.findViewById(R.id.txt_header), ConstantVariables.CommonValues.StringTimeline, true);
	}

	private void setNonEmptyDataToTimelineRecycler()
	{
		recyclerTimeline.setLayoutManager(new LinearLayoutManager(context));
		recyclerTimeline.setAdapter(new ViewTimelineAdapter(listTimeline, context));
	}

	private void initListeners()
	{
		fabAddFollowUp.setOnClickListener(v -> fragment.replace(new AddFollowupTimeLine(fragment)));
	}

	private void initVariables()
	{
		new BackButton(Objects.requireNonNull(getActivity()), view.findViewById(R.id.img_back), true);
		sessionManager = new SessionManager(context);
		networkCaller = new NetworkCaller(context, this);
		listTimeline = new ArrayList<>();
		progressDialogView = new ProgressDialogView(context);
	}

	private void initWidgets()
	{
		recyclerTimeline = view.findViewById(R.id.recycler_timeline);
		fabAddFollowUp = view.findViewById(R.id.fab_add_follow_up);
		noData = view.findViewById(R.id.noData);
	}

	private JsonObject getBodyForlistTimeline()
	{
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ConstantVariables.ApiValues.StringSpacePartnerLeadIdKey, sessionManager.getSpaceLeadId());
		jsonObject.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());

		Log.d(TAG, "getBodyForlistFollowup: jsonObject: " + jsonObject);
		return jsonObject;
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listTimelineIdentifier:
                if (ResponseValidator.isValidResponse(Response, context, false))
                {
                    ExtractDataForListTimeline(Response);
                }
                else
                {
                    NoTimeLineDataFound();
                }
				break;
		}
	}

	private void NoTimeLineDataFound()
	{
		noData.setVisibility(View.VISIBLE);
		recyclerTimeline.setVisibility(View.GONE);
	}
	private void HasTimeLineDataFound()
	{
		noData.setVisibility(View.GONE);
		recyclerTimeline.setVisibility(View.VISIBLE);
	}

	private void ExtractDataForListTimeline(String response)
	{
		try
		{
			ListTimelineResponse listTimelineResponse = new Gson().fromJson(response, ListTimelineResponse.class);
            if (listTimelineResponse.getData() != null && listTimelineResponse.getData().size() > 0)
            {
                listTimeline = listTimelineResponse.getData();
                setNonEmptyDataToTimelineRecycler();
				HasTimeLineDataFound();
            }
            else
            {
                NoTimeLineDataFound();
            }
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataForListTimeline: Error Extracting list Time line " + e.getLocalizedMessage());
			NoTimeLineDataFound();
		}
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listTimelineIdentifier:
				NoTimeLineDataFound();
				break;
		}
	}

	@Override
	public void StateChange(boolean status)
	{
		InternetStatus.getInstance().isOnline(context, view.findViewById(R.id.img_internet_status), true);
		if (status)
			getTimelineList();
	}
}
