package com.rm.office_bing.fragments.WalletFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.WalletAdapters.RechargeHistoryAdapter;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.WalletModel.ListRechargeHistory.ListRechargeDataItem;
import com.rm.office_bing.Models.WalletModel.ListRechargeHistory.ListRechargeResponse;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.databinding.FragmentRechargeBinding;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RechargeFragment extends Fragment implements ResponseCarrier
{
	private final int listRechargeIdentifier = 272;
	String TAG = "RechargeFragment";
	Context context;
	View view;
	NetworkCaller networkCaller;
	SessionManager sessionManager;
	ProgressDialogView progressDialogView;
	List<ListRechargeDataItem> listRechargeHistory;
	private FragmentRechargeBinding fragmentRechargeBinding;

	public RechargeFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		//return inflater.inflate(R.layout.fragment_recharge, container, false);
		fragmentRechargeBinding = FragmentRechargeBinding.inflate(getLayoutInflater());
		return fragmentRechargeBinding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		initViews();
		initListeners();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		initVariables();
		getWalletRechargeHistory();
	}

	private void getWalletRechargeHistory()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.WalletHistory, getBodyForRechargeHistory(), listRechargeIdentifier, true);
	}

	private JsonObject getBodyForRechargeHistory()
	{
		JsonObject getBodyForRechargeHistory = new JsonObject();
		getBodyForRechargeHistory.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		return getBodyForRechargeHistory;
	}

	private void initViews()
	{
	}

	private void initVariables()
	{
		networkCaller = new NetworkCaller(context, this);
		sessionManager = new SessionManager(context);
		progressDialogView = new ProgressDialogView(context);
		listRechargeHistory = new ArrayList<>();
	}

	private void initListeners()
	{
		fragmentRechargeBinding.txtNoData.setOnClickListener(v -> getWalletRechargeHistory());
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listRechargeIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractDataForRechargeHistory(Response);
				}
				else
				{
					NoData();
				}
				break;
		}
	}

	private void ExtractDataForRechargeHistory(String response)
	{
		try
		{
			ListRechargeResponse listRechargeResponse = new Gson().fromJson(response, ListRechargeResponse.class);
			if (listRechargeResponse.getData() != null && listRechargeResponse.getData().size() > 0)
			{
				HasData();
				listRechargeHistory = listRechargeResponse.getData();
				setNonEmptyDataTORechargeList();
			}
			else
			{
				NoData();
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataForRechargeHistory: Errror Extracting Data for recharge history");
			NoData();
		}

	}

	private void setNonEmptyDataTORechargeList()
	{
		fragmentRechargeBinding.recyclerRecharge.setLayoutManager(new LinearLayoutManager(context));
		fragmentRechargeBinding.recyclerRecharge.setAdapter(new RechargeHistoryAdapter(listRechargeHistory, context, sessionManager));
	}

	private void NoData()
	{
		fragmentRechargeBinding.txtNoData.setVisibility(View.VISIBLE);
		fragmentRechargeBinding.recyclerRecharge.setVisibility(View.GONE);
	}

	private void HasData()
	{
		fragmentRechargeBinding.txtNoData.setVisibility(View.GONE);
		fragmentRechargeBinding.recyclerRecharge.setVisibility(View.VISIBLE);
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listRechargeIdentifier:
				NoData();
				break;
		}
	}
}
