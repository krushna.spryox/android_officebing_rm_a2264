package com.rm.office_bing.fragments.WalletFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rm.office_bing.Adapters.WalletAdapters.TransactionAdapter;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.Models.WalletModel.ListTransactionHistory.ListTransactionDataItem;
import com.rm.office_bing.Models.WalletModel.ListTransactionHistory.ListTransactionResponse;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ResponseValidator;
import com.rm.office_bing.Utils.SessionManager;
import com.rm.office_bing.databinding.FragmentTransactionBinding;
import com.rm.office_bing.network.ApiNodes;
import com.rm.office_bing.network.NetworkCaller;

import java.util.ArrayList;
import java.util.List;


public class TransactionFragment extends Fragment implements ResponseCarrier
{
	private final int listPaymentIdentifier = 172;
	String TAG = "TransactionFragment";
	Context context;
	View view;
	NetworkCaller networkCaller;
	SessionManager sessionManager;
	ProgressDialogView progressDialogView;
	List<ListTransactionDataItem> listTransactions;
	private FragmentTransactionBinding fragmentTransactionBinding;

	public TransactionFragment()
	{
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		//return inflater.inflate(R.layout.fragment_transaction, container, false);
		fragmentTransactionBinding = FragmentTransactionBinding.inflate(getLayoutInflater());
		return fragmentTransactionBinding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		this.view = view;
		context = view.getContext();
		initViews();
		initListeners();
	}

	@Override
	public void onStart()
	{
		super.onStart();

	}

	@Override
	public void onResume()
	{
		super.onResume();
		initVariables();
		getWalletTransactions();
	}

	private void getWalletTransactions()
	{
		progressDialogView.showProgress();
		networkCaller.initPostCallWithBody(ApiNodes.PaymentHistory, getBodyForPaymentHistory(), listPaymentIdentifier, true);
	}

	private void initViews()
	{
	}

	private void initVariables()
	{
		networkCaller = new NetworkCaller(context, this);
		sessionManager = new SessionManager(context);
		progressDialogView = new ProgressDialogView(context);
		listTransactions = new ArrayList<>();
	}

	private void initListeners()
	{
		fragmentTransactionBinding.txtNoData.setOnClickListener(v -> getWalletTransactions());
	}

	@Override
	public void Success(String Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listPaymentIdentifier:
				if (ResponseValidator.isValidResponse(Response, context, false))
				{
					ExtractDataForTransaction(Response);
				}
				else
				{
					NoData();
				}
				break;
		}
	}

	private void ExtractDataForTransaction(String response)
	{
		try
		{
			ListTransactionResponse listTransactionResponse = new Gson().fromJson(response, ListTransactionResponse.class);
			if (listTransactionResponse.getData() != null && listTransactionResponse.getData().size() > 0)
			{
				HasData();
				listTransactions = listTransactionResponse.getData();
				setNonEmptyDataToList();
			}
			else
			{
				NoData();
			}
		}
		catch (Exception e)
		{
			Log.d(TAG, "ExtractDataForTransaction: Error Extracting Trasaction Data " + e.getLocalizedMessage());
			NoData();
		}


	}

	private void setNonEmptyDataToList()
	{
		fragmentTransactionBinding.recyclerTransaction.setLayoutManager(new LinearLayoutManager(context));
		fragmentTransactionBinding.recyclerTransaction.setAdapter(new TransactionAdapter(context, listTransactions));
	}

	@Override
	public void Error(Throwable Response, int Identifier)
	{
		progressDialogView.hideProgress();
		switch (Identifier)
		{
			case listPaymentIdentifier:
				NoData();
				break;
		}
	}

	private void NoData()
	{
		fragmentTransactionBinding.txtNoData.setVisibility(View.VISIBLE);
		fragmentTransactionBinding.recyclerTransaction.setVisibility(View.GONE);
	}

	private void HasData()
	{
		fragmentTransactionBinding.txtNoData.setVisibility(View.GONE);
		fragmentTransactionBinding.recyclerTransaction.setVisibility(View.VISIBLE);
	}

	public JsonObject getBodyForPaymentHistory()
	{
		JsonObject bodyForPaymentHistory = new JsonObject();
		bodyForPaymentHistory.addProperty(ConstantVariables.ApiValues.StringIdKey, sessionManager.getUserID());
		return bodyForPaymentHistory;
	}
}
