package com.rm.office_bing.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.rm.office_bing.Utils.ConstantVariables;
import com.rm.office_bing.Utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class APIClient
{

	static String TAG = "APIClient";
	private static Retrofit retrofit = null;
	SessionManager sessionManager;

	public static APIInterface getClient(boolean responseInString)
	{
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder().readTimeout(ConstantVariables.OKHTTPVariables.MaxTimeOutInMins, ConstantVariables.OKHTTPVariables.ConnectionTimeOutUnit).retryOnConnectionFailure(true).writeTimeout(ConstantVariables.OKHTTPVariables.MaxTimeOutInMins, ConstantVariables.OKHTTPVariables.ConnectionTimeOutUnit).retryOnConnectionFailure(true).callTimeout(ConstantVariables.OKHTTPVariables.MaxTimeOutInMins, ConstantVariables.OKHTTPVariables.ConnectionTimeOutUnit).retryOnConnectionFailure(true).addInterceptor(interceptor).build();

		if (responseInString)
		{
			retrofit = new Retrofit.Builder().baseUrl(ApiNodes.BaseUrl)
					.addConverterFactory(ScalarsConverterFactory.create())
					.addConverterFactory(GsonConverterFactory.create())
					.client(client).build();
		}
		else
		{
			retrofit = new Retrofit.Builder().baseUrl(ApiNodes.BaseUrl)
					.addConverterFactory(GsonConverterFactory.create()).client(client).build();
		}
		return retrofit.create(APIInterface.class);
	}


	public static JsonObject ApiRequest(Context ctx, JsonObject requestData)
	{
		JsonObject jsonObject = new JsonObject();
		//        jsonObject.addProperty("api_key", ConstantValues.APIKEY);
		//        jsonObject.addProperty("user_token", new SessionManager(ctx).getUserToken());
		//        requestData.add("auth", jsonObject);
		return requestData;

	}


	public static Map<String, String> GetHeader(@NotNull Context context, boolean hasBarear)
	{
		SessionManager sessionManager = new SessionManager(context);
		Map<String, String> map = new HashMap<>();
		if (hasBarear)
		{
			map.put(ConstantVariables.Headers.AUTHORIZATION_LABEL, ConstantVariables.Headers.BEARER_LABEL + sessionManager.getAPIToken());
			Log.e(TAG, "GetHeader: " + ConstantVariables.Headers.BEARER_LABEL + sessionManager.getAPIToken());
		}
		else
		{
			map.put(ConstantVariables.Headers.StringXApiKey, ConstantVariables.Headers.StringXApiValue);
		}
		return map;
	}

}
