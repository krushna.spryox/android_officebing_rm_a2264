package com.rm.office_bing.network;

import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface
{

	@POST
	Call<String> getStringResponse(@Url String url, @HeaderMap Map<String, String> map, @Body JsonObject body);
}
