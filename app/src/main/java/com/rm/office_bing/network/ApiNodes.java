package com.rm.office_bing.network;

public class ApiNodes
{
//	private static final String DevUrl = "http://officebing.dev.ganniti.com/api/";
	private static final String DevUrl = "http://ob-dev.dev.ganniti.com/api/";
	public static final String BaseUrl = DevUrl;
	public static final String Register = BaseUrl + "space-partner/register";
	public static final String Login = BaseUrl + "relationship-manager/login";
	public static final String MyLeads = BaseUrl + "relationship-manager/my-leads";
	public static final String MySpaces = BaseUrl + "space-partner/spaces";
	public static final String MyProfile = BaseUrl + "space-partner/profile";
	public static final String Plans = BaseUrl + "space-partner/plans";
	public static final String AcceptLead = BaseUrl + "space-partner/accept-lead";
	public static final String RejectLead = BaseUrl + "space-partner/decline-lead";
	public static final String LeadDetails = BaseUrl + "relationship-manager/lead-details";
	public static final String ListTimeline = BaseUrl + "relationship-manager/list-followup";
	public static final String ListReasons = BaseUrl + "relationship-manager/dcln-dspt-followup-status";
	public static final String AddFollowup = BaseUrl + "relationship-manager/add-followup";
	public static final String MarkDispute = BaseUrl + "space-partner/disputes";
	public static final String PaymentHistory = BaseUrl + "space-partner/payment-transaction-history";
	public static final String WalletHistory = BaseUrl + "space-partner/wallet-transaction-history";
	public static final String GetOrderDetails = BaseUrl + "space-partner/plan-transaction";
	public static final String PostPaymentUpdate = BaseUrl + "space-partner/update-plan-transaction";
	public static final String ForgotPassword = BaseUrl + "space-partner/forgot-password";
	public static final String ChangePassword = BaseUrl + "space-partner/change-password";



}
