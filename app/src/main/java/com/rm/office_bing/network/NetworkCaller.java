package com.rm.office_bing.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.rm.office_bing.Communicators.ResponseCarrier;
import com.rm.office_bing.ParentActivity.InternetStatus;
import com.rm.office_bing.Utils.ProgressDialogView;
import com.rm.office_bing.Utils.ToastMessages;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkCaller
{
	String TAG = "NetworkCaller";
	private Context context;
	private ResponseCarrier carrier;

	public NetworkCaller(Context context, ResponseCarrier carrier)
	{
		this.context = context;
		this.carrier = carrier;
	}

	public void initPostCallWithBody(String Url, JsonObject requestBody, final int Identifier, boolean WithBrearToken)
	{
		if (!InternetStatus.isNetwork(context))
		{
			Toast.makeText(context, ToastMessages.NoInternet, Toast.LENGTH_SHORT).show();
			new ProgressDialogView(context).hideProgress();
			carrier.Error(null, Identifier);
			return;
		}
		Call<String> caller = APIClient.getClient(true).getStringResponse(Url, APIClient.GetHeader(context, WithBrearToken), requestBody);
		caller.enqueue(new Callback<String>()
		{
			@Override
			public void onResponse(Call<String> call, Response<String> response)
			{
				Log.d(TAG, "onResponse:response " + response.body());
				Log.d(TAG, "onResponse:Identifier " + Identifier);
				carrier.Success(response.body(), Identifier);
			}

			@Override
			public void onFailure(Call<String> call, Throwable t)
			{
				carrier.Error(t, Identifier);
			}
		});
	}

}
