package com.rm.office_bing.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rm.office_bing.Activities.LoginActivity;
import com.rm.office_bing.R;
import com.rm.office_bing.Utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG ="MyFirebaseMessaging";
    SessionManager sessionManager;

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification() != null ? remoteMessage.getNotification().getBody() : "", remoteMessage.getNotification().getTitle(), remoteMessage.getData());
        } else {
            sendNotification(remoteMessage.getNotification() != null ? remoteMessage.getNotification().getBody() : "", "", remoteMessage.getData());
        }

    }

    @Override
    public void onNewToken(@NotNull String token) {
        Log.d(TAG, "Refreshed token: " + token);
        sessionManager = new SessionManager(this);
        sessionManager.setFcmToken(token);

    }

    private void sendNotification(String messageBody, String Title, Map<String, String> data) {
        Intent intent = null;

        if (data != null) {
            Title = data.containsKey("title") ? data.get("title") : getString(R.string.default_notification_channel_id);
            messageBody = data.containsKey("body") ? data.get("body") : "hello user";
        }
        if (intent == null) {
            switch (data.get("type")) {
                case "1":
                    intent = new Intent(this, LoginActivity.class);
                    //                    intent.putExtra(USER_ID, data.get("userid"));
                    //                    intent.putExtra("segment", data.get("segment"));
                    //                    intent.putExtra("singleSegment","true");
                    break;
                case "2":
                    intent = new Intent(this, LoginActivity.class);
                    //                    intent.putExtra("expiry_date", data.get("type_id"));
                    //                    intent.putExtra("registered_date", data.get("type_id"));
                    break;
                default:
                    intent = new Intent(this, LoginActivity.class);

            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId).setSmallIcon(R.mipmap.ic_launcher).setDefaults(Notification.DEFAULT_ALL).setContentTitle(!Title.equalsIgnoreCase("") ? Title : channelId).setContentText(!messageBody.equalsIgnoreCase("") ? messageBody : "hello user").setSound(defaultSoundUri).setPriority(Notification.PRIORITY_HIGH).setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());

    }

}
